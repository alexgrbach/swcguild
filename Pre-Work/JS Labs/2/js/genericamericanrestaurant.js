jQuery(function($){
	$("#textInputPhone").mask("(999)999-9999",{completed:function(){
		document.getElementById("textInputEmail").required = false;
		document.getElementById("textInputEmail").setCustomValidity('');
		document.getElementById("textInputPhone").setCustomValidity('');	
	}});
});	
jQuery('#textInputEmail').on('input', function() {
	document.getElementById("textInputPhone").required = false;
	document.getElementById("textInputPhone").setCustomValidity('');
    document.getElementById("textInputEmail").setCustomValidity('');
});
function moreInfo(){
	if (document.getElementById("reason").value == "Information")
		document.getElementById("additonalInfo").required = true;
	else
		document.getElementById("additonalInfo").required = false;
};
function checkBoxes(){
	if (document.getElementsByName("day")[0].checked == true || document.getElementsByName("day")[1].checked == true || document.getElementsByName("day")[2].checked == true || document.getElementsByName("day")[3].checked == true || document.getElementsByName("day")[4].checked == true){
		document.getElementById("check").setCustomValidity('');
	}
	else
		document.getElementById("check").setCustomValidity('Please select a day for us to contact you');
};