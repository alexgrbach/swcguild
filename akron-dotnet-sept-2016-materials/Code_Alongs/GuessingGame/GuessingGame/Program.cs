﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter your name: ");
            string name = Console.ReadLine();

            bool playAgain = false;
            do
            {
                Program something = new Program();
                something.PlayGame(name);

                Console.Write("Do you want to play again? (Y - yes, N - No) ");
                string response = Console.ReadLine();

                switch (response.ToUpper())
                {
                    case "Y":
                    case "YES":
                        playAgain = true;
                        break;
                    default:
                        playAgain = false;
                        break;
                }
            } while (playAgain);
        }

        public void PlayGame(string name)
        {
            Random rndm = new Random();
            int numberToBeGuessed = rndm.Next(1, 21);

            bool isValid = false;
            do
            {
                Console.Write($"{name}, Enter a number between 1 and 20 (q to quit): ");
                string input = Console.ReadLine();
                int numberGuessed;
                isValid = int.TryParse(input, out numberGuessed);

                if (isValid) // we have a number, it might be between 1-20, it might not...
                {
                    if (!(numberGuessed <= 20 && numberGuessed >= 1))
                    {
                        Console.WriteLine("The number needs to be between 1 and 20...");
                        isValid = false;
                    }
                    else
                    {
                        if (numberGuessed == numberToBeGuessed)
                        {
                            Console.WriteLine($"YOU WIN!!! yeah {name}...");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("NOPE, try again...");
                            isValid = false;
                        }
                    }
                }
                else // did they type q?
                {
                    if (input.ToUpper() == "Q")
                    {
                        break;  // since we do not do anything after the loop we can just return;
                    }

                    Console.WriteLine("REALLY!!! I said numbers... preferably 1-20...");
                }

            } while (!isValid);
        }
    }
}
