﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using SWCCorpADO.Models;

namespace SWCCorpADO.Data
{
    public class DapperRepository
    {
        private static string _connectionString =
            ConfigurationManager.ConnectionStrings["SWCCorp"].ConnectionString;

        private static string _connectionString2 =
    ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString;

        public List<Employee> GetAllEmployees()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                employees = cn.Query<Employee>(@"SELECT e.EmpID, e.FirstName, e.LastName
                                    FROM Employee e").ToList();
            }

            return employees;
        }

        public List<Employee> GetAllEmployeesByView()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                employees = cn.Query<Employee>(@"SELECT e.EmpID, e.FirstName, e.LastName
                                    FROM EmployeeGrants e").ToList();
            }

            return employees;
        }

        public List<Employee> GetAllEmployeesByStoredProc()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                employees = cn.Query<Employee>("spGetEmployeeGrants"
                    , commandType: CommandType.StoredProcedure).ToList();
            }

            return employees;
        }

        public List<Employee> GetAllEmployeesWithGrants()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                employees = cn.Query<Employee, Grant, Employee>(@"SELECT * FROM EmployeeGrants e",
                    (emp, grt) =>
                    {
                        Employee employee;
                        if ((employee = employees.FirstOrDefault(e => e.EmpId == emp.EmpId)) == null)
                        {
                            employees.Add(employee = emp);
                        }

                        if (employee.Grants == null)
                        {
                            employee.Grants = new List<Grant>();
                        }

                        employee.Grants.Add(grt);

                        return employee;
                    }, splitOn: "GrantId").ToList();
            }

            return employees;
        }

        public int InsertRegion(string description)
        {
            int regionId = 0;

            using (var cn = new SqlConnection(_connectionString2))
            {
                var parameters = new DynamicParameters();
                parameters.Add("RegionDescription", description);
                parameters.Add("RegionId", DbType.Int32, direction: ParameterDirection.Output);

                cn.Execute("RegionInsert", parameters, commandType: CommandType.StoredProcedure);

                regionId = parameters.Get<int>("RegionId");
            }

            return regionId;
        }
    }
}
