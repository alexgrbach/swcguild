﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SWCCorpADO.Models;

namespace SWCCorpADO.Data
{
    public class DBRepository
    {
        private static string _connectionString =
            ConfigurationManager.ConnectionStrings["SWCCorp"].ConnectionString;

        public List<Employee> GetAllEmployees()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = @"SELECT e.EmpID, e.FirstName, e.LastName
	                                    , g.GrantID, g.GrantName, g.Amount
                                    FROM Employee e
	                                    INNER JOIN [Grant] g
		                                    ON e.EmpID = g.EmpID
                                    ORDER BY g.Amount DESC";
                cmd.Connection = cn;

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Employee employee = employees.FirstOrDefault(e => e.EmpId == (int)dr["EmpId"]);
                        if (employee == null)
                        {
                            employee = ConvertToEmployee(dr);
                            employees.Add(employee);
                        }

                        Grant grant = ConvertToGrant(dr);

                        employee.Grants.Add(grant);
                    }
                }
            }

            return employees;
        }

        public List<Employee> GetAllEmployeesByView()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = @"SELECT *
                                    FROM EmployeeGrants";
                cmd.Connection = cn;

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Employee employee = employees.FirstOrDefault(e => e.EmpId == (int)dr["EmpId"]);
                        if (employee == null)
                        {
                            employee = ConvertToEmployee(dr);
                            employees.Add(employee);
                        }

                        Grant grant = ConvertToGrant(dr);

                        employee.Grants.Add(grant);
                    }
                }
            }

            return employees;
        }

        public List<Employee> GetAllEmployeesByStoredProc()
        {
            List<Employee> employees = new List<Employee>();

            using (var cn = new SqlConnection(_connectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "spGetEmployeeGrants";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Employee employee = employees.FirstOrDefault(e => e.EmpId == (int)dr["EmpId"]);
                        if (employee == null)
                        {
                            employee = ConvertToEmployee(dr);
                            employees.Add(employee);
                        }

                        Grant grant = ConvertToGrant(dr);

                        employee.Grants.Add(grant);
                    }
                }
            }

            return employees;
        }

        private Employee ConvertToEmployee(SqlDataReader dr)
        {
            return new Employee()
            {
                EmpId = (int)dr["EmpId"],
                FirstName = dr["FirstName"].ToString(),
                LastName = dr["LastName"].ToString(),
                Grants = new List<Grant>()
            };
        }

        private Grant ConvertToGrant(SqlDataReader dr)
        {
            return new Grant()
            {
                GrantId = int.Parse(dr["GrantId"].ToString()),
                GrantName = dr["GrantName"].ToString(),
                // This is to show you parsing instead of just casting
                Amount = decimal.Parse(dr["Amount"].ToString())
            };
        }
    }
}
