﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SWCCorpADO.Data;

namespace SWCCorpADO
{
    class Program
    {
        static void Main(string[] args)
        {
            //DisplayAllEmployees();
            //DisplayAllEmployeesWithDapper();
            InsertNewRegion();

            Console.ReadLine();
        }

        public static void DisplayAllEmployees()
        {
            var repo = new DBRepository();
            //var employees = repo.GetAllEmployees();
            //var employees = repo.GetAllEmployeesByView();
            var employees = repo.GetAllEmployeesByStoredProc();

            foreach (var employee in employees)
            {
                Console.WriteLine($"{employee.EmpId} - {employee.LastName}, {employee.FirstName}");

                Console.WriteLine("Grants");
                foreach (var grant in employee.Grants)
                {
                    Console.WriteLine($"\t{grant.GrantId} {grant.GrantName} - {grant.Amount:C}");
                }

                Console.WriteLine();
            }
        }

        public static void DisplayAllEmployeesWithDapper()
        {
            var repo = new DapperRepository();
            //var employees = repo.GetAllEmployees();
            //var employees = repo.GetAllEmployeesByView();
            //var employees = repo.GetAllEmployeesByStoredProc();
            var employees = repo.GetAllEmployeesWithGrants();

            foreach (var employee in employees)
            {
                Console.WriteLine($"{employee.EmpId} - {employee.LastName}, {employee.FirstName}");

                if (employee.Grants != null)
                {
                    foreach (var grant in employee.Grants)
                    {
                        Console.WriteLine($"\t{grant.GrantId} {grant.GrantName} - {grant.Amount:C}");
                    }
                }

                Console.WriteLine();
            }
        }

        public static void InsertNewRegion()
        {
            Console.Write("What region would like to insert: ");
            string region = Console.ReadLine();

            var repo = new DapperRepository();
            int regionId = repo.InsertRegion(region);

            Console.WriteLine($"Your new region has id {regionId}");
        }
    }
}
