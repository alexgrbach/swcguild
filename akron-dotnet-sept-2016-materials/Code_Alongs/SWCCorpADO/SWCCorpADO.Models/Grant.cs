﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWCCorpADO.Models
{
    public class Grant
    {
        public int GrantId { get; set; }
        public string GrantName { get; set; }
        public decimal Amount { get; set; }
    }
}
