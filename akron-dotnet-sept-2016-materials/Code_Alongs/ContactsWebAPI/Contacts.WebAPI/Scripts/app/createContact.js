﻿$(document)
    .ready(function() {
        // show the model
        $('#btnShowAddContact')
            .on('click',
                function() {
                    $('#addContactModal').modal('show');
                });

        // save event, send the data to the server
        $('#btnSaveContact')
            .on('click',
                function () {
                    // create a contact object to send to server
                    var contact = {};
                    contact.Name = $('#name').val();
                    contact.PhoneNumber = $('#phonenumber').val();

                    $.post(uri, contact)
                        .done(function() {
                            loadContacts();
                            $('#addContactModal').modal('hide');
                        })
                        .fail(function(jqXhr, status, err) {
                            alert(status + ' - ' + err);
                        });
                });

        // solves issue with data staying in modal after modal is hidden
        $('#addContactModal')
            .on('hidden.bs.modal',
                function() {
                    $('#name').val('');
                    $('#phonenumber').val('');
                });
    });