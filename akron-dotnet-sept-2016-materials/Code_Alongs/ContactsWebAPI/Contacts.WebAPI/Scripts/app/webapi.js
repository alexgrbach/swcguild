﻿var uri = '/api/contacts/';

$(document)
    .ready(function() {
        loadContacts();
    });

function loadContacts() {
    // Send a Get request to the WebAPI Controller
    $.getJSON(uri)
        .done(function (data) {
            // noticed that data is some kind of object array
            //alert(data);

            //clear the table
            $('#contacts tbody tr').remove();

            // data contains a list of contacts
            // we need to iterate each one and add it to the table
            $.each(data,
                function(index, contact) {
                    // add a row to the table for the contact
                    $(createRow(contact)).appendTo($('#contacts tbody'));
                });
        });
};

function createRow(contact) {
    return '<tr><td>' +
        contact.ContactID +
        '</td><td>' +
        contact.Name +
        '</td><td>' +
        contact.PhoneNumber +
        '</td></tr>';
};