﻿using FunWithCollections.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            NonGenerics.ShowArrayList();
            NonGenerics.ShowHashTable();
            NonGenerics.ShowStack();
            NonGenerics.ShowQueue();

            Generics gen = new Generics();
            gen.ShowStack();
            gen.SimpleList();
            gen.PersonDictionary();

            Console.ReadLine();
        }
    }
}
