﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JqueryFun.Models;

namespace JqueryFun.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ConfirmAppt(Appointment appt)
        {
            return View(appt);
        }

        public ActionResult DragDropSample()
        {
            return View();
        }
    }
}