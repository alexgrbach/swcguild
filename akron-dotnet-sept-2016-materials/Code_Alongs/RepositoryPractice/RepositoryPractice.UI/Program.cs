﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositoryPractice.Data;

namespace RepositoryPractice.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = AccountFactory.CreateRepository();

            var accounts = repo.GetAllAccounts();

            foreach (var account in accounts)
            {
                Console.WriteLine($"{account.AccountId} {account.Name} {account.Balance}");
            }

            Console.ReadLine();
        }
    }
}
