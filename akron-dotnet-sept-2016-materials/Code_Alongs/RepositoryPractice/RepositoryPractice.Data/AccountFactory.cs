﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPractice.Data
{
    public static class AccountFactory
    {
        public static IAccountRepository CreateRepository()
        {
            IAccountRepository repo;

            string mode = ConfigurationManager.AppSettings["mode"].ToString();
            switch(mode.ToUpper())
            {
                case "TEST":
                    repo = new MockAccountRepository();
                    break;
                case "PROD":
                    repo = new FileAccountRepository();
                    break;
                default:
                    throw new Exception("I don't know that mode!");
            }

            return repo;
        }
    }
}
