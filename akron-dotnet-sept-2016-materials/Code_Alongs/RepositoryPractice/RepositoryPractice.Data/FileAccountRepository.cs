﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositoryPractice.Models;
using System.IO;

namespace RepositoryPractice.Data
{
    public class FileAccountRepository : IAccountRepository
    {
        private const string FILEPATH = @"DataFiles\Accounts.txt";

        public List<Account> GetAllAccounts()
        {
            List<Account> accounts = new List<Account>();

            using(StreamReader sr = File.OpenText(FILEPATH))
            {
                string inputLine;
                while ((inputLine = sr.ReadLine()) != null)
                {
                    var accountPieces = inputLine.Split(',');
                    var account = new Account();
                    account.AccountId = int.Parse(accountPieces[0]);
                    account.Name = accountPieces[1];
                    account.Balance = decimal.Parse(accountPieces[2]);

                    accounts.Add(account);
                }
            }

            return accounts;
        }
    }
}
