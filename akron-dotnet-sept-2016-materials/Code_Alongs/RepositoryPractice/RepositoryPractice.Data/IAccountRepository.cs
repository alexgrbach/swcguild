﻿using RepositoryPractice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPractice.Data
{
    public interface IAccountRepository
    {
        List<Account> GetAllAccounts();
    }
}
