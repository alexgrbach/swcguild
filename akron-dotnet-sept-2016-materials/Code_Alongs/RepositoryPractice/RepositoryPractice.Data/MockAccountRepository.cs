﻿using RepositoryPractice.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPractice.Data
{
    public class MockAccountRepository : IAccountRepository
    {
        private static List<Account> _accounts;

        public MockAccountRepository()
        {
            if (_accounts == null)
            {
                _accounts = new List<Account>()
                {
                    new Account() { AccountId = 1, Name = "Bryant", Balance= 10000 },
                    new Account() { AccountId = 2, Name = "Ryan", Balance= 4000 },
                    new Account() { AccountId = 3, Name = "Jacob", Balance= 10000000 },
                    new Account() { AccountId = 4, Name = "Calli", Balance= 5 }
                };
            }
        }

        public List<Account> GetAllAccounts()
        {
            return _accounts;
        }
    }
}
