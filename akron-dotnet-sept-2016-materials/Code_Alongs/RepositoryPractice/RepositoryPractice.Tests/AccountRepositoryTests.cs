﻿using NUnit.Framework;
using RepositoryPractice.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPractice.Tests
{
    [TestFixture]
    class AccountRepositoryTests
    {
        [Test]
        public void GetAccountsTest()
        {
            var repo = AccountFactory.CreateRepository();
            var accounts = repo.GetAllAccounts();

            foreach(var account in accounts)
            {
                Console.WriteLine($"{account.AccountId} {account.Name} = {account.Balance}");
            }

            Assert.Greater(accounts.Count(), 0);
        }
    }
}
