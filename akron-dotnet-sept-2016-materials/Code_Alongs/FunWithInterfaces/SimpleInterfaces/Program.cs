﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            GermanShepherd bubble = new GermanShepherd();
            Console.WriteLine(bubble.GoForAWalk());
            bubble.Bark();
            bubble.Name = "Bubble";

            List<IDog> dogs = new List<IDog>();
            dogs.Add(bubble);
            dogs.Add(new Chihuahua("Butch"));

            Console.WriteLine();

            foreach (var dog in dogs)
            {
                Console.WriteLine($"{dog.Name} {dog.GoForAWalk()}");
                dog.Bark();
            }

            Console.ReadLine();
        }
    }
}
