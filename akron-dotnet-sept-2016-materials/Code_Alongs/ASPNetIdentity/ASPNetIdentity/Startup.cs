﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASPNetIdentity.Startup))]
namespace ASPNetIdentity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
