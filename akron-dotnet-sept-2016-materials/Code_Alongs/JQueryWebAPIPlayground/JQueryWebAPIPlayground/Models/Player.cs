﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JQueryWebAPIPlayground.Models
{
    public class Player
    {
        // properties of players
        public int Id { get; set; }
        public string LastName { get; set; }
        public int JerseyNumber { get; set; }

        // relating to the team the player is on
        //public int TeamId { get; set; }
        //public Team Team { get; set; }
    }
}