﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JQueryWebAPIPlayground.Models
{
    public class MockRepository
    {
        public static List<Team> Teams { get; set; }
        public static List<Player> Players { get; set; }

        static MockRepository()
        {
            Players = new List<Player>()
            {
                new Player() {Id = 1, LastName = "Lindor", JerseyNumber = 12},
                new Player() {Id = 2, LastName = "Kipnis", JerseyNumber = 22},
                new Player() {Id = 3, LastName = "Miller", JerseyNumber = 24},
                new Player() {Id = 4, LastName = "Bryant", JerseyNumber = 17},
                new Player() {Id = 5, LastName = "Schramber", JerseyNumber = 27}
            };

            Teams = new List<Team>()
            {
                new Team()
                {
                    Id = 1,
                    Name = "Indians",
                    Players = new List<Player>()
                    //{
                    //    Players.FirstOrDefault(p=>p.Id == 1)
                    //}
                },
                new Team()
                {
                    Id = 2,
                    Name = "Cubs",
                    Players = new List<Player>()
                }
            };
        }
    }
}