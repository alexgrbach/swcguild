﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JQueryWebAPIPlayground.Models
{
    public class Team
    {
        // properties of the team
        public int Id { get; set; }
        public string Name { get; set; }

        // one to many relationship with players
        public List<Player> Players { get; set; }
    }
}