﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JQueryWebAPIPlayground.Models
{
    public class TeamSelectionVM
    {
        public Team TeamToEdit { get; set; }
        public List<SelectListItem> Players { get; set; }
        public List<SelectListItem> PlayersOnTeam { get; set; }
        public List<int> PlayersToAdd { get; set; }

        public void SetPlayerList(List<Player> players)
        {
            this.Players = ConvertPlayerToSelectListItem(players);
        }

        public void SetTeamRoster(List<Player> players)
        {
            this.PlayersOnTeam = ConvertPlayerToSelectListItem(players);
        }

        private List<SelectListItem> ConvertPlayerToSelectListItem(List<Player> players)
        {
            var newCollection = new List<SelectListItem>();
            foreach (var player in players)
            {
                newCollection.Add(new SelectListItem()
                {
                    Text = player.JerseyNumber + " " + player.LastName,
                    Value = player.Id.ToString()
                });
            }

            return newCollection;
        }

    }
}