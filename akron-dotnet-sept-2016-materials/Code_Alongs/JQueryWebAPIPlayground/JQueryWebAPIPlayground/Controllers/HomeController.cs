﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JQueryWebAPIPlayground.Models;

namespace JQueryWebAPIPlayground.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MVCListBox()
        {
            // Need to create a new view model
            var vm = new TeamSelectionVM();

            // Need to get the team I want to display
            vm.TeamToEdit = MockRepository.Teams.FirstOrDefault(t => t.Id == 1);

            // filter out the players who are already on the team
            var players = MockRepository.Players.Except(vm.TeamToEdit.Players).ToList();

            // convert the available players and roster to selectlistitems
            vm.SetPlayerList(players);
            vm.SetTeamRoster(vm.TeamToEdit.Players);

            // show the page
            return View(vm);
        }

        [HttpPost]
        public ActionResult MVCListBox(TeamSelectionVM vm)
        {
            // need to retrieve the team
            // when I posted, I only gave the ID
            // The team name and players and any other information was not in the form
            var team = MockRepository.Teams.FirstOrDefault(t => t.Id == vm.TeamToEdit.Id);
            team.Players.Clear();

            // validation check to make sure there are players to add
            if (vm.PlayersToAdd != null && vm.PlayersToAdd.Count > 0)
            {
                // for every play to add
                // note the players to add are only ids at this point
                // only the ids were pposted from the form. 
                foreach (var playerid in vm.PlayersToAdd)
                {
                    // add a play to the team
                    // remember to get the player object to add that object and not just the id
                    team.Players.Add(MockRepository.Players.FirstOrDefault(p => p.Id == playerid));
                }
            }

            // go back to edit, because why not?!?
            return RedirectToAction("MVCListBox");
        }
    }
}