﻿using System.Collections.Generic;
using DVDLibrary.Models;

namespace DvdLibrary.Data
{
    public interface IActorRepository
    {
        List<Actor> GetActors();
        Actor GetActorById(int id);
        Actor CreateActor(Actor model);
        void DeleteActor(int id);
        void UpdateActor(int id, Actor model);
    }
}