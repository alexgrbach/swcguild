﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDLibrary.Models;

namespace DvdLibrary.Data
{
    public class DvdContext : DbContext
    {
        public DvdContext():base("dvdContext")
        {
            
        }

        public DbSet<Dvd> Dvds { get; set; }
        public DbSet<Cast> Casts { get; set; }
        public DbSet<Actor> Actors { get; set; }
    }
}
