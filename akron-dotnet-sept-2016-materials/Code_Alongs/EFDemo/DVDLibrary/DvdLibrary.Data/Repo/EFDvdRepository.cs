﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDLibrary.Models;

namespace DvdLibrary.Data.Repo
{
    public class EFDvdRepository : IDvdRepository
    {
        public List<Dvd> GetDvds()
        {
            throw new NotImplementedException();
        }

        public Dvd GetDvdById(int id)
        {
            throw new NotImplementedException();
        }

        public Dvd CreateDvd(Dvd model)
        {
            throw new NotImplementedException();
        }

        public void DeleteDvd(int id)
        {
            throw new NotImplementedException();
        }

        public void UpdateDvd(int id, Dvd model)
        {
            throw new NotImplementedException();
        }
    }
}
