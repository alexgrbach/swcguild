﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Permissions;
using DVDLibrary.Models;

namespace DvdLibrary.Data.Repo
{
    public class CastRepository : ICastRepository
    {
        private SqlConnection sqlConnection;

        public CastRepository(string connection)
        {
            this.sqlConnection = new SqlConnection(connection);

        }
        public CastRepository()
        {
            this.sqlConnection = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=DvdLibraryADO;Integrated Security=True");
        }
        

        public List<Cast> GetCastsByDvdId(int dvdId)
        {
            List<Cast> casts = new List<Cast>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select Id,ActorId,DvdId, Role From casts where dvdId = @dvdId", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@dvdId", dvdId);
                SqlDataReader reader = cmd.ExecuteReader();
                
                while (reader.Read())
                {
                    Cast cast = new Cast
                    {
                        Id = reader.GetInt32(0),
                        ActorId = reader.GetInt32(1),
                        DvdId = reader.GetInt32(2),
                        Role = reader.GetString(3)
                    };
                    casts.Add(cast);
                }
            }
            sqlConnection.Close();
            return casts;

        }

        public Cast GetCastById(int id)
        {

            List<Cast> casts = new List<Cast>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select Id,ActorId,DvdId, Role From casts where id = @dvdId", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Cast cast = new Cast
                    {
                        Id = reader.GetInt32(0),
                        ActorId = reader.GetInt32(1),
                        DvdId = reader.GetInt32(2),
                        Role = reader.GetString(3)
                    };
                    casts.Add(cast);
                }
            }
            sqlConnection.Close();
            return casts.FirstOrDefault();
        }

        public Cast CreateCast(Cast model)
        {
            using (SqlCommand cmd = new SqlCommand("CreateCast",sqlConnection))
            {
                sqlConnection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter pID = new SqlParameter("ID", SqlDbType.Int);

                pID.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pID);

                cmd.Parameters.AddWithValue("@actorId", model.ActorId);
                cmd.Parameters.AddWithValue("@dvdId", model.DvdId);
                cmd.Parameters.AddWithValue("@role", model.Role);
                cmd.ExecuteScalar();
                int id = Convert.ToInt32(cmd.Parameters["ID"].Value.ToString());
                model.Id = id;
                sqlConnection.Close();
            }
            return model;
        }

        public void DeleteCast(int id)
        {
            using (SqlCommand cmd = new SqlCommand("Delete from Casts where id = @id", sqlConnection))
            {
                sqlConnection.Open();


                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
           
                sqlConnection.Close();
            }
        }

        public void UpdateCast(int id, Cast model)
        {
            using (SqlCommand cmd = new SqlCommand("Update Cast Set Role = @role,actorId = @actorId, dvdId = @dvdId where id = @id", sqlConnection))
            {
                sqlConnection.Open();
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.AddWithValue("@actorId", model.ActorId);
                cmd.Parameters.AddWithValue("@dvdId", model.DvdId);
                cmd.Parameters.AddWithValue("@role", model.Role);
                cmd.Parameters.AddWithValue("@id", model.Id);
                cmd.ExecuteNonQuery();
                sqlConnection.Close();
            }
        }
    }
}