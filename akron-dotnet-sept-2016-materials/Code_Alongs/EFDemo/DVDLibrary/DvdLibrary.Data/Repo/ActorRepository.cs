﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DVDLibrary.Models;

namespace DvdLibrary.Data.Repo
{
    public class ActorRepository : IActorRepository
    {
        private SqlConnection sqlConnection;

        public ActorRepository()
        {
            sqlConnection = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=DvdLibraryADO;Integrated Security=True");
        }
        public List<Actor> GetActors()
        {
            List<Actor> actors = new List<Actor>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select * From Actors", sqlConnection))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Actor actor = new Actor()
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                    actors.Add(actor);

                }
            }
            sqlConnection.Close();
            return actors;
        }

        public Actor GetActorById(int id)
        {

            List<Actor> actors = new List<Actor>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select * From Actors Where id = @id", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Actor actor = new Actor()
                    {
                        Id = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                    actors.Add(actor);

                }
            }
            sqlConnection.Close();
            return actors.FirstOrDefault();
        }

        public Actor CreateActor(Actor model)
        {

            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("CreateActor", sqlConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter pID = new SqlParameter("ID", SqlDbType.Int);

                pID.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pID);


                cmd.Parameters.AddWithValue("@Name", model.Name);

                cmd.ExecuteScalar();
                int id = Convert.ToInt32(cmd.Parameters["ID"].Value.ToString());
                model.Id = id;
            }
            sqlConnection.Close();
            return model;
        }

        public void DeleteActor(int id)
        {

            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("delete from Actors where id = @id", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
            sqlConnection.Close();
        }

        public void UpdateActor(int id, Actor model)
        {

            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Update Actor Set Name = @name where id = @id ", sqlConnection))
            {

                cmd.Parameters.AddWithValue("@id", id);


                cmd.Parameters.AddWithValue("@name", model.Name);

                cmd.ExecuteNonQuery();
            }
            sqlConnection.Close();
        }
    }
}