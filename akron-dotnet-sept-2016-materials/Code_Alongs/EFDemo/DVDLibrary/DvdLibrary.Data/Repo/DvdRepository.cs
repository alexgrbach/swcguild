﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DVDLibrary.Models;

namespace DvdLibrary.Data.Repo
{
    public class DvdRepository : IDvdRepository
    {
        private SqlConnection sqlConnection;

        public DvdRepository(string connection)
        {
            this.sqlConnection = new SqlConnection(connection);

        }
        public DvdRepository()
        {
            this.sqlConnection = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=DvdLibraryADO;Integrated Security=True");
        }

        public List<Dvd> GetDvds()
        {
            List<Dvd> dvds = new List<Dvd>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select * From Dvds", sqlConnection))
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Dvd dvd = new Dvd
                    {
                        Id = reader.GetInt32(0),
                        Title = reader.GetString(1),
                        ReleaseDate = reader.GetDateTime(2)
                    };
                    dvds.Add(dvd);
                }
            }
            sqlConnection.Close();
            return dvds;
        }

        public Dvd GetDvdById(int id)
        {
            List<Dvd> dvds = new List<Dvd>();
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Select * From Dvds Where Id = @Id", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@Id", id);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Dvd dvd = new Dvd
                    {
                        Id = reader.GetInt32(0),
                        Title = reader.GetString(1),
                       ReleaseDate= reader.GetDateTime(2)
                    };
                    dvds.Add(dvd);
                }
            }
            sqlConnection.Close();
            return dvds.FirstOrDefault();
        }

        public Dvd CreateDvd(Dvd model)
        {
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("CreateDvd", sqlConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter pID = new SqlParameter("ID", SqlDbType.Int);

                pID.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(pID);


                cmd.Parameters.AddWithValue("@Title", model.Title);
                cmd.Parameters.AddWithValue("@ReleaseDate", model.ReleaseDate);
                
                cmd.ExecuteScalar();
                int id = Convert.ToInt32(cmd.Parameters["ID"].Value.ToString());
                model.Id = id;
            }
            sqlConnection.Close();
            return model;
        }

        public void DeleteDvd(int id)
        {
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Delete From Dvds Where Id = @Id", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@Id", id);
                 cmd.ExecuteNonQuery();
            
            }
            sqlConnection.Close();
        }

        public void UpdateDvd(int id, Dvd model)
        {
            sqlConnection.Open();
            using (SqlCommand cmd = new SqlCommand("Update Dvds Set Title = @Title, ReleaseDate = @ReleaseDate  Where Id = @Id", sqlConnection))
            {
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@Title", model.Title);
                cmd.Parameters.AddWithValue("@ReleaseDate", model.ReleaseDate);
                cmd.ExecuteNonQuery();

            }
            sqlConnection.Close();
        }
    }
}