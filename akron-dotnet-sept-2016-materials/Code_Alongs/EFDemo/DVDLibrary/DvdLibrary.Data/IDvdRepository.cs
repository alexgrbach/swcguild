﻿using DVDLibrary.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DvdLibrary.Data
{
    public interface IDvdRepository
    {
        List<Dvd> GetDvds();
        Dvd GetDvdById(int id);
        Dvd CreateDvd(Dvd model);
        void DeleteDvd(int id);
        void UpdateDvd(int id, Dvd model);
    }
}
