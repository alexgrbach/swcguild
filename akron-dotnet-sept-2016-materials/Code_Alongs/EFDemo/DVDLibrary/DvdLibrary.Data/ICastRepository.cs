﻿using System.Collections.Generic;
using DVDLibrary.Models;

namespace DvdLibrary.Data
{
    public interface ICastRepository
    {
        List<Cast> GetCastsByDvdId(int dvdId);
        Cast GetCastById(int id);
        Cast CreateCast(Cast model);
        void DeleteCast(int id);
        void UpdateCast(int id, Cast model);
    }
}