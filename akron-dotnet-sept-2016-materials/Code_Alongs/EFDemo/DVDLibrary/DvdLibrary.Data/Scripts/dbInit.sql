USE [master]
GO
/****** Object:  Database [DvdLibraryADO]    Script Date: 11/18/2016 9:13:44 AM ******/
CREATE DATABASE [DvdLibraryADO]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DvdLibraryADO', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DvdLibraryADO.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DvdLibraryADO_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DvdLibraryADO_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DvdLibraryADO] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DvdLibraryADO].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DvdLibraryADO] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET ARITHABORT OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DvdLibraryADO] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DvdLibraryADO] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DvdLibraryADO] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DvdLibraryADO] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DvdLibraryADO] SET  MULTI_USER 
GO
ALTER DATABASE [DvdLibraryADO] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DvdLibraryADO] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DvdLibraryADO] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DvdLibraryADO] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DvdLibraryADO] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DvdLibraryADO]
GO
/****** Object:  Table [dbo].[Actors]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Actors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Actors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Casts]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Casts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DvdId] [int] NOT NULL,
	[ActorId] [int] NOT NULL,
	[Role] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Casts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Dvds]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Dvds](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[ReleaseDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Dvds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Casts]  WITH CHECK ADD  CONSTRAINT [FK_Casts_Actors] FOREIGN KEY([ActorId])
REFERENCES [dbo].[Actors] ([Id])
GO
ALTER TABLE [dbo].[Casts] CHECK CONSTRAINT [FK_Casts_Actors]
GO
ALTER TABLE [dbo].[Casts]  WITH CHECK ADD  CONSTRAINT [FK_Casts_Dvds] FOREIGN KEY([DvdId])
REFERENCES [dbo].[Dvds] ([Id])
GO
ALTER TABLE [dbo].[Casts] CHECK CONSTRAINT [FK_Casts_Dvds]
GO
/****** Object:  StoredProcedure [dbo].[CreateActor]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[CreateActor] 
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@ID int output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
	Insert into Actors(Name) Values( @name); 
	
	Set @ID = SCOPE_IDENTITY() 
	
	END

GO
/****** Object:  StoredProcedure [dbo].[CreateCast]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
Create PROCEDURE [dbo].[CreateCast] 
	-- Add the parameters for the stored procedure here
	@actorId int, 
	@dvdId int, 
	@role varchar(50),
	@ID int output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


    -- Insert statements for procedure here
	Insert into Casts(actorId,dvdId,[role]) Values( @actorId, @dvdId,@role); 
	
	Set @ID = SCOPE_IDENTITY() 
	
	END

GO
/****** Object:  StoredProcedure [dbo].[CreateDvd]    Script Date: 11/18/2016 9:13:44 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[CreateDvd] 
	-- Add the parameters for the stored procedure here
	@Title varchar(50) , 
	@ReleaseDate datetime,
	@ID int output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into Dvds (Title,ReleaseDate) Values( @Title, @ReleaseDate); 
	
	Set @ID = SCOPE_IDENTITY() 
	
	END

GO
USE [master]
GO
ALTER DATABASE [DvdLibraryADO] SET  READ_WRITE 
GO
