﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DvdLibrary.Data;
using DvdLibrary.Data.Repo;
using DVDLibrary.Models;

namespace DvdLibrary.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            IDvdRepository dvdRepository = new DvdRepository();
            IActorRepository actorRepository = new ActorRepository();
            ICastRepository castRepository = new CastRepository();

            var actor1 = actorRepository.CreateActor(new Actor() { Name = "Kristen Bell" });
            var actor2 = actorRepository.CreateActor(new Actor() { Name = "Idina Menzel" });
            var newDvd = new Dvd() { Title = "Frozen", ReleaseDate = new DateTime(2013, 10, 19) };
            Dvd dvd = dvdRepository.CreateDvd(newDvd);

            var cast1 = castRepository.CreateCast(new Cast() { ActorId = actor1.Id, DvdId = dvd.Id, Role = "Anna" });
            var cast2 = castRepository.CreateCast(new Cast() { ActorId = actor2.Id, DvdId = dvd.Id, Role = "Elsa" });
            //IActorRepository actorRepository;
            //ICastRepository castRepository;
            
            Console.WriteLine($"Title: {dvd.Title}");
            Console.WriteLine($"Release Date: {dvd.ReleaseDate.ToShortDateString()}");
            Console.WriteLine($"Actors: ");
            foreach (var cast in castRepository.GetCastsByDvdId(dvd.Id))
            {
                var actor = actorRepository.GetActorById(cast.ActorId);
                Console.WriteLine($"\t Name: {actor.Name}");
                Console.WriteLine($"\t Role: {cast.Role}");
            }
            Console.ReadLine();
            dvd.Title = "Let it go!";
            dvdRepository.UpdateDvd(dvd.Id, dvd);
            castRepository.DeleteCast(cast1.Id);
            castRepository.DeleteCast(cast2.Id);
            actorRepository.DeleteActor(actor1.Id);
            actorRepository.DeleteActor(actor2.Id);
            dvdRepository.DeleteDvd(dvd.Id);

        }
    }
}
