﻿using System.Collections.Generic;

namespace DVDLibrary.Models
{
    public class Cast
    {
        public int Id { get; set; }
        public int ActorId { get; set; }
        public int DvdId { get; set; }
        public string Role { get; set; }

        public virtual List<Actor> Actors { get; set; }
        public virtual List<Dvd> Dvds { get; set; }
    }
    

}