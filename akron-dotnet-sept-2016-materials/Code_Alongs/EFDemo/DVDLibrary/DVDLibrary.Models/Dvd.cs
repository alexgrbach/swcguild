﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Dvd
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public virtual List<Cast> Casts { get; set; }

        public Dvd()
        {
            this.Casts = new List<Cast>();
        }
    }
}
