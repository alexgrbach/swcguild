﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Cast> Casts { get; set; }
    }
}
