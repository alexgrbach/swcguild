﻿using SGBank.Data;
using SGBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.BLL
{
    public class AccountOperations
    {
        public AccountResponse MakeWithdrawal(Account account, decimal amountToWithdraw)
        {
            AccountResponse response = new AccountResponse();

            var isSuccessful = false;

            var repo = AccountFactory.CreateAccountRepository();

            var source = repo.GetAccountByNumber(account.AccountNumber, account.BranchName);
            if (source != null)
            {
                if (source.Balance >= amountToWithdraw)
                {
                    isSuccessful = repo.Withdrawal(source, amountToWithdraw);

                    if (isSuccessful)
                    {
                        response.Success = true;
                        response.AccountInfo = source;
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Withdraw failed";
                    }
                }
                else
                {
                    response.Success = false;
                    response.Message = "Not enough money in the account";
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Could not find account";
            }

            return response;
        }

        public AccountResponse GetAccount(string accountNumber, string branchName)
        {
            AccountResponse response = new AccountResponse();

            var repo = AccountFactory.CreateAccountRepository();
            var account = repo.GetAccountByNumber(accountNumber, branchName);

            if (account != null)
            {
                response.Success = true;
                response.AccountInfo = account;
            }
            else
            {
                response.Success = false;
                response.Message = $"No Account found for account number: {accountNumber}";
            }

            return response;
        }

        public AccountResponse OpenAccount(Account account)
        {
            AccountResponse response = new AccountResponse();

            var repo = AccountFactory.CreateAccountRepository();
            var accounts = repo.GetAccounts(account.BranchName);

            // get the next available account number
            var accountNumberMax = 0;
            if (accounts.Count > 0)
            {
                accountNumberMax = int.Parse(accounts.Max(a => a.AccountNumber));
            }

            account.AccountNumber = (accountNumberMax + 1).ToString();

            if (repo.Open(account))
            {
                response.Success = true;
                response.AccountInfo = account;
            }
            else
            {
                response.Success = false;
                response.Message = "Failed to create new account...";
            }

            return response;
        }

        public AccountResponse UpdateAccount(Account account)
        {
            AccountResponse response = new AccountResponse();

            var repo = AccountFactory.CreateAccountRepository();

            if (repo.Update(account))
            {
                response.Success = true;
                response.AccountInfo = account;
            }
            else
            {
                response.Success = false;
                response.Message = "Failed to create new account...";
            }

            return response;
        }
    }
}
