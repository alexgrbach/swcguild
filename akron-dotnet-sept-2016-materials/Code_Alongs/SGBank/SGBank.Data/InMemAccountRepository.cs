﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;

namespace SGBank.Data
{
    public class InMemAccountRepository : IAccountRepository
    {
        private static List<Account> _accounts;

        public InMemAccountRepository()
        {
            if (_accounts == null)
            {
                _accounts = new List<Account>()
                {
                    new Account()
                    {
                        AccountNumber = "123456",
                        Name = "Checking",
                        Balance = 1234.56m,
                        FirstName = "Victor",
                        LastName = "Pudelski",
                        BranchName = "DotNet"
                    },
                    new Account()
                    {
                        AccountNumber = "234567",
                        Name = "Savings",
                        Balance = 1.99m,
                        FirstName = "Randall",
                        LastName = "Clapper",
                        BranchName = "DotNet"
                    },
                    new Account()
                    {
                        AccountNumber = "345678",
                        Name = "Whatever",
                        Balance = 4321543.99m,
                        FirstName = "Sarah",
                        LastName = "Dutkiewicz",
                        BranchName = "JAVA"
                    },
                    new Account()
                    {
                        AccountNumber = "456789",
                        Name = "Another",
                        Balance = 32178.56m,
                        FirstName = "Dave",
                        LastName = "Balzer",
                        BranchName = "DotNet"
                    }
                };
            }
        }

        public List<Account> GetAccounts(string branchName)
        {
            return _accounts.Where(a => a.BranchName == branchName).ToList();
        }

        public Account GetAccountByNumber(string accountNumber, string branchName)
        {
            return _accounts.FirstOrDefault(a => a.AccountNumber == accountNumber && a.BranchName == branchName);
        }

        public bool Withdrawal(Account account, decimal amountToWithdraw)
        {
            bool isSuccessful = false;

            var source = GetAccountByNumber(account.AccountNumber, account.BranchName);
            if (source != null)
            {
                source.Balance -= amountToWithdraw;
                isSuccessful = true;
            }

            return isSuccessful;
        }

        public bool Open(Account account)
        {
            _accounts.Add(account);

            return true;
        }

        public bool Update(Account account)
        {
            var accountToRemove = _accounts.FirstOrDefault(a => a.AccountNumber == account.AccountNumber);
            _accounts.Remove(accountToRemove);
            _accounts.Add(account);
            _accounts = _accounts.OrderBy(x => x.AccountNumber).ToList();

            return true;
        }
    }
}
