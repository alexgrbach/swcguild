﻿using SGBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.Data
{
    public interface IAccountRepository
    {
        /// <summary>
        /// get all the accounts currently available
        /// </summary>
        /// <param name="branchName">name of the file to search</param>
        /// <returns>list of all accounts</returns>
        List<Account> GetAccounts(string branchName);

        /// <summary>
        /// get the account by the designated number
        /// </summary>
        /// <param name="accountNumber">account number to retrieve</param>
        /// <param name="branchName">name of the file to search</param>
        /// <returns>account object representing the number</returns>
        Account GetAccountByNumber(string accountNumber, string branchName);

        /// <summary>
        /// take money from the account
        /// </summary>
        /// <param name="account">where to get the money</param>
        /// <param name="amountToWithdraw">how much to get</param>
        /// <returns>did it work?</returns>
        bool Withdrawal(Account account, decimal amountToWithdraw);

        /// <summary>
        /// create a new account
        /// </summary>
        /// <param name="account">object representing our new account</param>
        /// <returns>did it work?</returns>
        bool Open(Account account);

        /// <summary>
        /// will update the existing account with new info
        /// </summary>
        /// <param name="account">the account to update</param>
        /// <returns>did it work?</returns>
        bool Update(Account account);
    }
}
