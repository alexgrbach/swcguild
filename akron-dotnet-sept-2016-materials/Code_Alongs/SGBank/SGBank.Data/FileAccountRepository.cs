﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;

namespace SGBank.Data
{
    public class FileAccountRepository : IAccountRepository
    {
        // my file name is going to be accounts_<branch>.txt -> accounts_DotNet.txt
        private const string FILENAME = @"DataFiles\accounts_";
        private const string FILEEXT = ".txt";

        public List<Account> GetAccounts(string branchName)
        {
            var accounts = ReadFromFile(branchName);
            return accounts;
        }

        public Account GetAccountByNumber(string accountNumber, string branchName)
        {
            var accounts = ReadFromFile(branchName);
            return accounts.FirstOrDefault(a => a.AccountNumber == accountNumber);
        }

        public bool Withdrawal(Account account, decimal amountToWithdraw)
        {
            bool isSuccess = false;

            
            var accounts = ReadFromFile(account.BranchName);
            var accountToChange = accounts.FirstOrDefault(a => a.AccountNumber == account.AccountNumber);

            if (accountToChange != null)
            {
                accountToChange.Balance -= amountToWithdraw;

                var fileName = FILENAME + accountToChange.BranchName + FILEEXT;
                WriteToFile(accounts, fileName);

                isSuccess = true;
            }

            return isSuccess;
        }

        public bool Open(Account account)
        {
            var accounts = ReadFromFile(account.BranchName);
            accounts.Add(account);

            var fileName = FILENAME + account.BranchName + FILEEXT;

            WriteToFile(accounts, fileName);

            return true;
        }

        public bool Update(Account account)
        {
            var accounts = ReadFromFile(account.BranchName);
            var accountToRemove = accounts.FirstOrDefault(a => a.AccountNumber == account.AccountNumber);
            accounts.Remove(accountToRemove);
            accounts.Add(account);
            accounts = accounts.OrderBy(a => a.AccountNumber).ToList();

            var fileName = FILENAME + account.BranchName + FILEEXT;

            WriteToFile(accounts, fileName);

            return true;
        }

        private List<Account> ReadFromFile(string branchName)
        {
            var fileName = FILENAME + branchName + FILEEXT;

            List<Account> accounts = new List<Account>();

            if (File.Exists(fileName))
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    // read the header line
                    sr.ReadLine();

                    string inputLine = "";
                    while ((inputLine = sr.ReadLine()) != null)
                    {
                        string[] inputParts = inputLine.Split(',');
                        Account newAccount = new Account()
                        {
                            AccountNumber = inputParts[0],
                            Name = inputParts[1],
                            FirstName = inputParts[2],
                            LastName = inputParts[3],
                            Balance = decimal.Parse(inputParts[4]),
                            BranchName = branchName
                        };

                        accounts.Add(newAccount);
                    }
                }
            }

            return accounts;
        }

        private void WriteToFile(List<Account> accounts, string FileName)
        {
            using (StreamWriter sw = new StreamWriter(FileName, false))
            {
                // write the header line
                sw.WriteLine("ACCOUNTNUMBER,NAME,FIRSTNAME,LASTNAME,BALANCE");

                foreach (var account in accounts)
                {
                    sw.WriteLine($"{account.AccountNumber},{account.Name},{account.FirstName},{account.LastName},{account.Balance}");
                }
            }
        }
    }
}
