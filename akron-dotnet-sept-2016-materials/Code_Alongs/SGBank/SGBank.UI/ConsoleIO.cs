﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.UI
{
    public class ConsoleIO
    {
        public static string GetStringFromUser(string message)
        {
            string input = "";

            Console.Clear();
            Console.Write(message);
            input = Console.ReadLine();

            return input;
        }
    }
}
