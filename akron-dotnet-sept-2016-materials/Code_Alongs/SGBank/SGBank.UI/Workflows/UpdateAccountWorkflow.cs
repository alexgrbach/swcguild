﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    public class UpdateAccountWorkflow
    {
        public Account Execute(Account account)
        {
            // display info to update and allow user to change
            var newAccount = PromptUserForUpdates(account);

            // process the update
            var accountToReturn = ProcessUpdate(newAccount);

            if (accountToReturn == null)
            {
                accountToReturn = account;
            }

            return accountToReturn;
        }

        private Account PromptUserForUpdates(Account account)
        {
            Account newAccount = new Account();

            newAccount.AccountNumber = account.AccountNumber;
            newAccount.Balance = account.Balance;

            newAccount.Name = PromptForStringValue(account.Name, "Name");
            newAccount.FirstName = PromptForStringValue(account.FirstName, "First Name");
            newAccount.LastName = PromptForStringValue(account.LastName, "Last Name");

            return newAccount;
        }

        private string PromptForStringValue(string currentValue, string field)
        {
            string newValue = "";

            Console.WriteLine($"{field} has current value of {currentValue}");
            Console.Write("What is the new value (enter to keep): ");
            newValue = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(newValue))
            {
                newValue = currentValue;
            }

            return newValue;
        }

        private Account ProcessUpdate(Account account)
        {
            var ops = new AccountOperations();
            var response = ops.UpdateAccount(account);

            if (!response.Success)
            {
                Console.WriteLine("Error Occurred!!!!");
                Console.WriteLine(response.Message);
                Console.ReadLine();
            }

            return response.AccountInfo;
        }
    }
}
