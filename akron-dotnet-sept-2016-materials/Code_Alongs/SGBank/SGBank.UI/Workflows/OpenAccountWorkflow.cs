﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    public class OpenAccountWorkflow
    {
        public void Execute()
        {
            // get name of the account
            // get the first, last name of the account
            // get an opening balance
            var account = PromptUserForNewAccount();

            if (ConfirmAccount(account))
            {
                ProcessOpenAccount(account);
            }
        }

        private Account PromptUserForNewAccount()
        {
            Account account = new Account();

            Console.Clear();
            account.BranchName = PromptUserForString("What is the name of the Branch: ");
            account.Name = PromptUserForString("What is the name of the account: ");
            account.FirstName = PromptUserForString("What is the first name of the person on the account: ");
            account.LastName = PromptUserForString("What is the last name of the person on the account: ");
            account.Balance = PromptUserForDecimal("What is the opening balance of the account: ");

            return account;
        }

        /// <summary>
        /// Method will prompt user for string and won't return until something is entered...
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private string PromptUserForString(string message)
        {
            string value = "";

            do
            {
                Console.Write(message);
                value = Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(value));

            return value;
        }

        /// <summary>
        /// method will prompt for decimal value and won't return until valid decimal is received...
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private decimal PromptUserForDecimal(string message)
        {
            decimal value = 0;
            string userInput = "";

            do
            {
                userInput = PromptUserForString(message);
            } while (!decimal.TryParse(userInput, out value));

            return value;
        }

        private bool ConfirmAccount(Account account)
        {
            bool confirm = false;

            LookupWorkflow.DisplayAccountInformation(account);

            Console.Write("Would you like to save this account: (Y/N) ");
            var input = Console.ReadLine();

            switch (input.ToUpper())
            {
                case "Y":
                case "YES":
                    confirm = true;
                    break;
                default:
                    confirm = false;
                    break;
            }

            return confirm;
        }

        public void ProcessOpenAccount(Account account)
        {
            var ops = new AccountOperations();
            var response = ops.OpenAccount(account);

            if (!response.Success)
            {
                Console.WriteLine("Error Occurred!!!!");
                Console.WriteLine(response.Message);
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine($"Your new account number is: {response.AccountInfo.AccountNumber}");
                Console.WriteLine("Press Enter to return to menu");
                Console.ReadLine();
            }
        }
    }
}
