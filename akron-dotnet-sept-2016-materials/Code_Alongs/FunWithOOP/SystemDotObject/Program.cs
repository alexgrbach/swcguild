﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    class Program
    {
        static void Main(string[] args)
        {
            Point p = new Point(11, 17);
            Console.WriteLine(p);

            Point p2 = new Point(11, 17);
            Console.WriteLine(object.Equals(p, p2));
            Console.WriteLine(p.Equals(p2));
            Console.WriteLine(object.ReferenceEquals(p, p2));

            Point p3 = p.Copy();
            Console.WriteLine(object.Equals(p, p3));
            Console.WriteLine(p.Equals(p3));
            Console.WriteLine(object.ReferenceEquals(p, p3));

            Console.ReadLine();
        }
    }
}
