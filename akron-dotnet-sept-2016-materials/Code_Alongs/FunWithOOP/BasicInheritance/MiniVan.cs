﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class MiniVan : Car
    {
        // default constructor calling Car(int)
        public MiniVan() : base(80)
        {
            // to illustrate which is called first 
            // base or derived
            MaxSpeed = 75;
            // base constructor called first
            // then the code here is called
            // hence maxspeed is 75
        }
    }
}
