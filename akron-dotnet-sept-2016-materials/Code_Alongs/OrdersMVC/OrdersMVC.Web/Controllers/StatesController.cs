﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersMVC.BLL;
using OrdersMVC.Models;

namespace OrdersMVC.Web.Controllers
{
    public class StatesController : Controller
    {
        // GET: States
        public ActionResult Index()
        {
            var ops = new OrderOperations();
            var states = ops.GetAllStates();

            return View(states);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(List<State> states)
        {
            var ops = new OrderOperations();

            foreach (var state in states)
            {
                ops.AddState(state);
            }

            return RedirectToAction("Index");
        }
    }
}