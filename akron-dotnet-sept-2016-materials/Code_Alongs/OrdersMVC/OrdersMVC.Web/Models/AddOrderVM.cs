﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OrdersMVC.Models;

namespace OrdersMVC.Web.Models
{
    public class AddOrderVM
    {
        public Order NewOrder { get; set; }
        public List<SelectListItem> States { get; set; }

        public void LoadStates(List<State> states)
        {
            States = new List<SelectListItem>();

            foreach (var state in states)
            {
                var item = new SelectListItem() {Text = state.Name, Value = state.Abbreviation};
                States.Add(item);
            }
        }
    }
}