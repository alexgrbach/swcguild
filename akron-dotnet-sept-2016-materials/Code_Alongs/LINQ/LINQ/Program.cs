﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            string userOption = "";
            do
            {
                Console.Clear();

                Console.Write("Select exercise to run: ");
                userOption = Console.ReadLine();

                int exercise;
                if (int.TryParse(userOption, out exercise))
                {
                    switch (exercise)
                    {
                        case 1:
                            PrintOutOfStock();
                            break;
                        case 2:
                            InStockMoreThan3();
                            break;
                        case 9:
                            ReturnBLessThanC();
                            break;
                        case 12:
                            First3OrderInWash();
                            break;
                        case 16:
                            ReturnValueLessThanPosition();
                            break;
                        case 24:
                            GroupThemByYearMonth();
                            break;
                        case 25:
                            ListDistinctCategories();
                            break;
                        case 26:
                            UniqueAandB();
                            break;
                        case 33:
                            AllProductsInCategoryInStock();
                            break;
                        default:
                            Console.WriteLine("Invalid Entry!");
                            break;
                    }

                    Console.WriteLine("press enter to continue...");
                }
                else
                {
                    if (userOption.ToUpper() != "Q")
                    {
                        Console.WriteLine("Invalid Entry, press enter to continue...");
                    }
                }

                if (userOption.ToUpper() != "Q")
                {
                    Console.ReadLine();
                }

            } while (userOption.ToUpper() != "Q");
        }

        //1. Find all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock == 0);
            var results = from p in products
                where p.UnitsInStock == 0
                select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        //2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {

        }

        //9. Make a query that returns all pairs of numbers from both arrays such 
        //   that the number from numbersB is less than the number from numbersC.
        private static void ReturnBLessThanC()
        {
            int[] numbersb = DataLoader.NumbersB;
            int[] numbersc = DataLoader.NumbersC;

            //int i = -1;
            //var results = from b in numbersb
            //    let index = i++
            //    where b < numbersc[i]
            //    select new {b, c = numbersc[i]};

            var results = numbersb.Select((b, i) => new {b, c = numbersc[i]})
                .Where(x => x.b < x.c);

            foreach (var result in results)
            {
                Console.WriteLine($"{result.b} - {result.c}");
            }
        }

        // 12. Get only the first 3 orders from customers in Washington.
        private static void First3OrderInWash()
        {
            var customers = DataLoader.LoadCustomers();

            //var results = (from c in customers
            //               from o in c.Orders
            //               where c.Region == "WA"
            //               select new { c.CompanyName, o.OrderID, o.OrderDate }).Take(3);
            var results = (customers.SelectMany(c => c.Orders, (c, o) => new {c, o})
                .Where(@t => @t.c.Region == "WA")
                .Select(@t => new {@t.c.CompanyName, @t.o.OrderID, @t.o.OrderDate})).Take(3);

            foreach (var o in results)
            {
                Console.WriteLine($"{o.OrderDate} {o.OrderID} - {o.CompanyName}");
            }
        }

        // 16. Return elements starting from the beginning of NumbersC 
        //     until a number is hit that is less than its position in the array.
        private static void ReturnValueLessThanPosition()
        {
            var arr = DataLoader.NumbersC;

            var results = arr.TakeWhile((n, index) => n >= index);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        // 24. Group customer orders by year, then by month.
        private static void GroupThemByYearMonth()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                          select new
                          {
                              CompanyName = c.CompanyName,
                              YearGroups = from o in c.Orders
                                           group o by o.OrderDate.Year
                                  into yg
                                           select new
                                           {
                                               Year = yg.Key,
                                               MonthGroups = from o in yg
                                                             group o by o.OrderDate.Month
                                                   into mg
                                                             select new
                                                             {
                                                                 Month = mg.Key,
                                                                 Orders = mg
                                                             }
                                           }
                          };
            //var results =
            //    customers.Select(c => new
            //    {
            //        CompanyName = c.CompanyName,
            //        YearGroups = c.Orders
            //            .GroupBy(o => o.OrderDate.Year)
            //            .Select(yg => new
            //            {
            //                Year = yg.Key,
            //                MonthGroups = yg.GroupBy(o => o.OrderDate.Month)
            //                    .Select(mg => new
            //                    {
            //                        Month = mg.Key,
            //                        Orders = mg
            //                    })
            //            })
            //    });

            foreach (var result in results)
            {
                Console.WriteLine(result.CompanyName);

                foreach (var yg in result.YearGroups)
                {
                    Console.WriteLine("\t{0}", yg.Year);

                    foreach (var mg in yg.MonthGroups)
                    {
                        Console.WriteLine("\t\t{0}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(mg.Month));

                        foreach (var order in mg.Orders)
                        {
                            Console.WriteLine("\t\t\t{0} - {1}", order.OrderDate, order.OrderID);
                        }
                    }
                }
            }
        }

        // 25. Create a list of unique product category names.
        private static void ListDistinctCategories()
        {
            var products = DataLoader.LoadProducts();

            var results = (from p in products
                           select p.Category).Distinct();

            //var results = products.Select(p => p.Category).Distinct();

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        // 26. Get a list of unique values from NumbersA and NumbersB.
        private static void UniqueAandB()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var results = numbersA.Union(numbersB);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        // 33. Get a grouped a list of products only for categories that have all of their products in stock.
        private static void AllProductsInCategoryInStock()
        {
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                into pCats
                          where pCats.All(x => x.UnitsInStock > 0)
                          select new { pCats.Key, coll = pCats };

            //var results = products.GroupBy(cat => cat.Category).Where(p => p.All(x => x.UnitsInStock > 0));

            foreach (var result in results)
            {
                Console.WriteLine("{0}", result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t{0} - {1}", product.UnitsInStock, product.ProductName);
                }
            }
        }
    }
}
