﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkCodeFirst.Data;
using EntityFrameworkCodeFirst.Models;

namespace EntityFrameworkCodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BlogContext())
            {

                var results = db.Blogs.OrderBy(b => b.Name);

                foreach (var result in results)
                {
                    Console.WriteLine($"{result.BlogId} {result.Name}");
                }

                Console.WriteLine("Enter Id of Blog or 'N' for new blog");
                var option = Console.ReadLine();

                Blog blog = null;
                int blogId;
                if (!int.TryParse(option, out blogId) || (blog = db.Blogs.FirstOrDefault(b => b.BlogId == blogId)) == null)
                {
                    Console.Write("Enter a name for a blog: ");
                    var name = Console.ReadLine();

                    blog = new Blog() {Name = name};
                    db.Blogs.Add(blog);
                    db.SaveChanges();
                }

                Console.WriteLine();

                Console.Write("What is the name of the new post? ");
                var postTitle = Console.ReadLine();
                Console.WriteLine("Type your post text here:");
                var postContent = Console.ReadLine();

                var post = new Post()
                {
                    Title = postTitle,
                    Content = postContent,
                };

                blog.Posts = new List<Post>();
                blog.Posts.Add(post);

                db.SaveChanges();

                foreach (var item in blog.Posts)
                {
                    Console.WriteLine(item.Title);
                }

                Console.WriteLine("Add tags separated by a space:");
                var tagsInput = Console.ReadLine();
                var tags = tagsInput.Split(' ');

                foreach (var tag in tags)
                {
                    var tagObject = new Tag()
                    {
                        Name = tag,
                        Description = tag
                    };

                    post.Tags.Add(tagObject);
                }

                db.SaveChanges();

                Console.WriteLine();
                foreach (var b in db.Blogs)
                {
                    Console.WriteLine($"{b.Name} {b.Url}");
                    foreach (var p in b.Posts)
                    {
                        Console.WriteLine($"\t{p.Title}");
                        foreach (var t in p.Tags)
                        {
                           Console.WriteLine($"\t\t{t.Name}");
                        }

                        Console.WriteLine();
                    }

                    Console.WriteLine();
                }
            }

            Console.ReadLine();
        }
    }
}
