﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCodeFirst.Models
{
    public class Tag
    {
        public Tag()
        {
            this.Posts = new List<Post>();
        }

        [Key]
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual List<Post> Posts { get; set; }
    }
}
