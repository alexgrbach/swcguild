// <auto-generated />
namespace EntityFrameworkCodeFirst.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addURL : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addURL));
        
        string IMigrationMetadata.Id
        {
            get { return "201612051536501_addURL"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
