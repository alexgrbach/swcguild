namespace EntityFrameworkCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tagname25max : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Tags");
            AlterColumn("dbo.Tags", "Name", c => c.String(nullable: false, maxLength: 25));
            AddPrimaryKey("dbo.Tags", "Name");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Tags");
            AlterColumn("dbo.Tags", "Name", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Tags", "Name");
        }
    }
}
