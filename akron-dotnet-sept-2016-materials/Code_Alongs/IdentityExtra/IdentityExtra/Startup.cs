﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IdentityExtra.Startup))]
namespace IdentityExtra
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
