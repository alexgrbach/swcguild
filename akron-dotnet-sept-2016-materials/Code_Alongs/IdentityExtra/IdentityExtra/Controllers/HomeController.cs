﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IdentityExtra.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // only allow logged in users to access this page
        [Authorize]
        public ActionResult LoggedInOnly()
        {
            return View();
        }

        // only allow user with the role admin on this page
        [Authorize(Roles = "Admin")]
        public ActionResult AdminOnly()
        {
            return View();
        }
    }
}