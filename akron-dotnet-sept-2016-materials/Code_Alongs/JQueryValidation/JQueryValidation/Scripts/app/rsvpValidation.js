﻿$(document)
    .ready(function() {
        $('#rsvpForm')
            .validate({
                rules: {
                    Name: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Phone: {
                        required: true,
                        phoneUS: true
                    },
                    FavoriteGame: {
                        required: true
                    },
                    WillAttend: {
                        required: true
                    }
                },
                messages: {
                    Name: {
                        required: "Please enter your Name"
                    },
                    Email: {
                        required: "Enter you Email",
                        email: "NOT THE RIGHT FORMAT!"
                    },
                    Phone: {
                        required: "Enter you phone",
                        phoneUS: "Not valid format"
                    },
                    FavoriteGame: {
                        required: "Enter a game"
                    },
                    WillAttend: {
                        required: "Are you coming, what gives?"
                    }
                }
            });
    });