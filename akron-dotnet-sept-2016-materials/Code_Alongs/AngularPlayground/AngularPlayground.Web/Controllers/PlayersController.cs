﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Friends.Models;

namespace AngularPlayground.Web.Controllers
{
    public class PlayersController : ApiController
    {
        public List<Player> Get()
        {
            var players = new List<Player>
            {
                new Player() {Number = 12, Name = "Lindor", Position = 6},
                new Player() {Number = 22, Name = "Kipnis", Position = 4},
                new Player() {Number = 41, Name = "Santana", Position = 3}
            };

            return players;
        }
    }
}
