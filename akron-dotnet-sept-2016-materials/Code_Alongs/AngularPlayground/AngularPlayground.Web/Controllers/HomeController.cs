﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularPlayground.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Directives()
        {
            return View();
        }

        public ActionResult Repeat()
        {
            return View();
        }

        public ActionResult Filters()
        {
            return View();
        }

        public ActionResult Modules()
        {
            return View();
        }

        public ActionResult ModulesWithWebAPI()
        {
            return View();
        }

        public ActionResult Routes()
        {
            return View();
        }

        /// <summary>
        /// This is a page to demonstrate the angular validation possible
        /// </summary>
        /// <returns>The view of the page for angular validation</returns>
        public ActionResult Validation()
        {
            return View();
        }

        /// <summary>
        /// This is a page to demonstrate the ability to create your own directives in Angular. 
        /// Here we will look at the example of a movie ratings object. 
        /// </summary>
        /// <returns>the view containing the directive in action</returns>
        public ActionResult CustomDirective()
        {
            return View();
        }
    }
}