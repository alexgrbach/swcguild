﻿// our typical angular module declaration. 
// This time we are including the ngRoute module as well. 
var app = angular.module('movieApp', ['ngRoute']);

// We are create a directive and not just a controller so notice the difference here. 
// we need to give the directive a name and also a set of json data detailing the directive. 
app.directive('ngStarRating',
    function () {
        // directives return an object
        return {
            restrict: 'EA',             // declare as element or attribute
            templateUrl: '/Content/app/templates/starRating.html',
                                        // where to find the html to draw for the directive
            scope: {
                rating: '=rating',      // two-way binding (get and set)
                maxRatingVal: '@',      // attribute of the directive
                label: '@',             // attribute of the directive
                click: "&",             // function
                mouseHover: "&",        // function
                mouseLeave: "&"         // function
            },
            // what to do when the directive is drawn
            compile: function (element, attrs) {
                // if they don't provide a maxRating attribute, default it to 5
                if (!attrs.maxRatingVal || Number(attrs.maxRatingVal) <= 0) {
                    attrs.maxRatingVal = '5';
                }

                if (!attrs.label) {
                    attrs.label = 'Rating';
                }
            },
            // the controller of the directive
            controller: function ($scope, $element, $attrs) {
                // array for the number of items to draw
                $scope.maxRatings = [];

                // initialization of rating variables
                // rating holds the clicked value
                // _rating holds the current value
                $scope.rating = $scope._rating = 0;

                // adding a variable to hold the hover value
                $scope.hoverValue = 0;

                // add an element for each rating
                // (used in ng-repeat in the template)
                for (var i = 1; i <= $scope.maxRatingVal; i++) {
                    $scope.maxRatings.push({});
                }

                // if the rating is less than the index (position) of the star
                // we should display an empty star
                // otherwise display a filled star
                $scope.getStarPath = function (index) {
                    if ($scope._rating + $scope.hoverValue <= index) {
                        return "/Content/app/images/star-empty-24.png";
                    }

                    return "/Content/app/images/star-full-24.png";
                };

                $scope.starClick = function (starValue) {
                    // set the rating
                    $scope.rating = $scope._rating = starValue;

                    // clear the hover value
                    $scope.hoverValue = 0;

                    $scope.click({ starValue: starValue }); // send to parent
                };

                // method to get the rating value and display in the span we created for the directive
                $scope.getRating = function () {
                    if ($scope.rating && $scope.rating !== 0) {
                        return $attrs.label + ": " + $scope.rating;
                    }

                    return $attrs.label;
                };

                // method to reset the hover value so that as we hover our control updates the stars correctly
                $scope.starMouseHover = function (starValue) {
                    $scope._rating = 0;

                    // set the hover value
                    $scope.hoverValue = starValue;

                    $scope.mouseHover({
                        starValue: starValue
                    });
                };

                // when you leave a star reset the star appropriately
                $scope.starMouseLeave = function (starValue) {
                    $scope._rating = $scope.rating;

                    // clear the hover value
                    $scope.hoverValue = 0;

                    $scope.mouseLeave({
                        starValue: starValue
                    });
                };

            }
        };
    });