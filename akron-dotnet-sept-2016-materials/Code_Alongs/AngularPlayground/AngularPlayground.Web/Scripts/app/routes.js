﻿var myApp = angular.module('friendsApp', ['ngRoute']);

myApp.factory('friendsFactory',
    function($http) {
        // create a new object
        var webAPIProvider = {};

        // url of our WebAPI Controller
        var url = '/api/friends/';

        // add a method to get all the friends
        webAPIProvider.getFriends = function() {
            return $http.get(url);
        };

        // add a method to add a friend
        webAPIProvider.saveFriend = function(friend) {
            return $http.post(url, friend);
        };

        // return our wrapped API object
        return webAPIProvider;
    });

// this is configuring the routes that we wish to use
// in our application. 
myApp.config(function ($routeProvider) {
    // when we get to the route specified set these options.
    $routeProvider.when('/Routes',
        {
            controller: 'FriendsController',
            templateUrl: '/AngularViews/FriendList.html'
        })
        .when('/AddFriend',
        {
            controller: 'AddFriendController',
            templateUrl: '/AngularViews/AddFriend.html'
        })
        // if no route is recognized do this
        .otherwise({ redirectTo: '/Routes' });
});

// controller for the get list.
myApp.controller('FriendsController',
    function ($scope, friendsFactory) {
        // call the get friends method as defined above. 
        friendsFactory.getFriends()
            .success(function (data) {
                // if the call succeeds set the scope 
                // variable called friends.
                $scope.friends = data;
            })
            .error(function(data, status) {
                alert('oh crap! status: ' + status);
            });
    });

// controller for the add friend
myApp.controller('AddFriendController',
    function ($scope, $location, friendsFactory) {
        // model bind a new object
        $scope.friend = {};

        // define a method for saving. 
        // this method is not immediately called
        // but instead can be tied to a ng-click event.
        $scope.save = function () {

            //call the save friend method as defined above
            friendsFactory.saveFriend($scope.friend)
                .success(function () {
                    // redirect the path to Routes
                    $location.path('/Routes');
                })
                .error(function(data, status) {
                    alert('oh crap! status: ' + status);
                });
        };
    });