﻿// this is a typical angular declaration to establish the angular module
// this code does nothing more than define the angular module and a controller.
// this is required for the logic of the validation to work. 
var app = angular.module('validationApp', []);

app.controller('ValidationController',
    function () {

    });