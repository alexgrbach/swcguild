﻿var myApp = angular.module('playersApp', []);

// factory will create and setup web API provider
myApp.factory('playersFactory',
    function($http) {
        // create a new object
        var webAPIProvider = {};

        // URL variable
        var url = '/api/players';

        // add a method to get the players
        webAPIProvider.getPlayers = function() {
            return $http.get(url);
        };

        return webAPIProvider;
    });

// controller to get the list of players
myApp.controller('playerController',
    function ($scope, playersFactory) {
        // call the http.get method from our http object
        playersFactory.getPlayers()
            .success(function(data) {
                // if the call (http.get) succeeds set the scope variable
                // data represents what is returned from the get
                $scope.players = data;
            });
    });