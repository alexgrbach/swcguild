Angular Playground Modifications

Validation
-----------
Steps
1. Added the Controller method and the View

2. Add a Render section to the _Layout page. 
This section is for styles and so it is at the top rather than at the bottom. Remember CSS goes in head tag..

3. Create a new CSS for the styles when the data is good and bad. These should be marked with the classes ng-valid.ng-dirty and ng-invalid.ng-dirty.

4. Add the reference to the stylesheet to the view

5. Create a javascript file declaring the angular module and controller.

6. Add the reference to the javascript to the view

7. Create your HTML and set the validation. 
here are some references for Angular Validation:
http://www.w3schools.com/angular/angular_validation.asp
https://docs.angularjs.org/guide/forms



Custom Directive - Movie Ratings
-----------------------------------
1. Create the controller method and view

2. On the view we will have a div for the Angular module and within another div for the custom directive.

3. Add a javascript file for the directive making the proper reference in the view. Also note you will need a reference to angular-route

4. Add a folder to Content for images

5. Add a folder to Content for templates

6. Add the 3 star images provided to the images folder

7. Create an html file representing the new directive and add it to the templates folder

8. In the star rating file create a div for the control and a span for displaying the rating. see the page for more notes...

9. In the script file we created we need to declare the angular module and specify everything for our directive. 
You will need a json definition of what the object looks like
You will also need to set the methods that you will need on the directive. 

** Custom Directives are not common. This is just an example of how powerful Angular is. 