﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZachsWebApp.Models;

namespace ZachsWebApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repo = new FileRepository();
            var people = repo.GetAll();

            return View(people);
        }

        public ActionResult Person(int id)
        {
            var repo = new FileRepository();
            var person = repo.GetById(id);

            return View(person);
        }
    }
}