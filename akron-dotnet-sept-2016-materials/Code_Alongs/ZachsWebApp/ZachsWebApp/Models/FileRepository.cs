﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ZachsWebApp.Models
{
    public class FileRepository
    {
        private string _people = "App_Data/people.txt";
        private string _careers = "App_Data/careers.txt";

        public FileRepository()
        {
            // This code solves the problem between web and local file system.
            if (HttpContext.Current != null)
            {
                _people = HttpContext.Current.Server.MapPath("~/" + _people);
                _careers = HttpContext.Current.Server.MapPath("~/" + _careers);
            }
        }

        private List<Occupation> GetAllCareers()
        {
            List<Occupation> careers = new List<Occupation>();

            if (File.Exists(_careers))
            {
                using (var reader = File.OpenText(_careers))
                {
                    //read the header line
                    reader.ReadLine();

                    string inputLine;
                    while ((inputLine = reader.ReadLine()) != null)
                    {
                        var columns = inputLine.Split(',');
                        var occupation = new Occupation()
                        {
                            Id = int.Parse(columns[0]),
                            Title = columns[1],
                            
                        };

                        careers.Add(occupation);
                    }
                }
            }

            return careers;
        }

        public List<Person> GetAll()
        {
            List<Person> people = new List<Person>();

            List<Occupation> careers = GetAllCareers();

            if (File.Exists(_people))
            {
                using (var reader = File.OpenText(_people))
                {
                    //read the header line
                    reader.ReadLine();

                    string inputLine;
                    while ((inputLine = reader.ReadLine()) != null)
                    {
                        var columns = inputLine.Split(',');
                        var order = new Person()
                        {
                            Id = int.Parse(columns[0]),
                            FirstName = columns[1],
                            LastName = columns[2],
                            Career = new Occupation()
                            {
                                Id = int.Parse(columns[3]),
                                Title = careers.FirstOrDefault(c=>c.Id == int.Parse(columns[3])).Title
                            }
                        };

                        people.Add(order);
                    }
                }
            }

            return people;
        }

        public Person GetById(int id)
        {
            return GetAll().FirstOrDefault(c => c.Id == id);
        }

        //public Order Add(Order newOrder)
        //{
        //    var orders = GetAll();
        //    orders.Add(newOrder);

        //    WriteFile(orders);

        //    return newOrder;
        //}

        //public bool Delete(Order orderToDelete)
        //{
        //    var orders = GetAll();
        //    orders.RemoveAll(c => c.Id == orderToDelete.Id);

        //    WriteFile(orders);

        //    return true;
        //}

        //public Order Edit(Order orderToUpdate)
        //{
        //    var orders = GetAll();
        //    orders.RemoveAll(c => c.Id == orderToUpdate.Id);
        //    orders.Add(orderToUpdate);

        //    WriteFile(orders);

        //    return orderToUpdate;
        //}

        //private void WriteFile(List<Order> orders)
        //{
        //    orders = orders.OrderBy(c => c.Id).ToList();
        //    using (var writer = new StreamWriter(_fileName, false))
        //    {
        //        writer.WriteLine("Id,OrderDate,CustomerName,StateAbbr,StateName,Total");

        //        foreach (Order order in orders)
        //        {
        //            writer.WriteLine($"{order.Id},{order.OrderDate.ToString("MM/dd/yyyy")},{order.CustomerName},{order.State.Abbreviation},{order.State.Name},{order.Total}");
        //        }
        //    }
        //}

    }
}