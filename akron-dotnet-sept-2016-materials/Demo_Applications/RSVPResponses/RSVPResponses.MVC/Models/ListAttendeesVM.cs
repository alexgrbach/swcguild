﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RSVPResponses.Models;

namespace RSVPResponses.MVC.Models
{
    public class ListAttendeesVM
    {
        // need to add a property to the view model for which checkboxes are checked
        public DaysOfTheWeek Checkboxes { get; set; }
        
        // property that is the existing model, if not already using a view model
        public List<Attendee> Attendees { get; set; }
    }
}