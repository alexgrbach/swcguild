﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSVPResponses.Data;
using RSVPResponses.Models;
using RSVPResponses.MVC.Models;

namespace RSVPResponses.MVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repo = new AttendeeRepository();
            
            //change the model being passed to View
            ListAttendeesVM vm = new ListAttendeesVM();
            vm.Attendees = repo.GetAll();

            return View(vm);
        }

        [HttpPost]
        public ActionResult FilterResult(DaysOfTheWeek checkboxes)
        {
            var repo = new AttendeeRepository();

            //change the model being passed to View
            ListAttendeesVM vm = new ListAttendeesVM();
            var attendees = repo.GetAll();

            if (checkboxes != 0)
            {
                attendees = attendees.Where(a => (a.Days & checkboxes) > 0).ToList();
            }

            vm.Attendees = attendees;
            vm.Checkboxes = checkboxes;

            return View("Index", vm);
        }

        public ActionResult AddRSVP()
        {
            var gameRepo = new GameRepository();

            var model = new AttendeeVM();
            model.CreateGameList(gameRepo.GetAll());

            return View(model);
        }

        [HttpPost]
        public ActionResult AddRSVP(AttendeeVM attendee)
        {
            var repo = new AttendeeRepository();
            repo.Add(attendee.Guest);

            return RedirectToAction("Index");
        }
    }
}