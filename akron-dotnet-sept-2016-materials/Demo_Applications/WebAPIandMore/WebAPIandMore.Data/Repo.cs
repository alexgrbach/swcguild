﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPIandMore.Models;

namespace WebAPIandMore.Data
{
    public class Repo
    {
        public static List<Student> Students { get; set; }
        public static List<Course> Courses { get; set; }

        static Repo()
        {
            var ryan = new Student()
            {
                Id = 1,
                Name = "Ryan",
                Courses = new List<Course>()
            };
            var christian = new Student()
            {
                Id = 2,
                Name = "Christian",
                Courses = new List<Course>()
            };

            Students = new List<Student>()
            {
                ryan, christian
            };


            var intro = new Course()
            {
                Id = 1,
                Credits = 4,
                Name = "Intro to Programming"
            };
            var ds = new Course()
            {
                Id = 2,
                Credits = 3,
                Name = "Data Structures"
            };
            var git = new Course()
            {
                Id = 3,
                Credits = 4,
                Name = "Git"
            };

            Courses = new List<Course>()
            {
                intro, ds, git
            };
            
            ryan.Courses.AddRange(new List<Course>() {intro, ds});
            christian.Courses.AddRange(new List<Course>() {intro, git});
        }
    }
}
