﻿// this is intializing our angular application
// declaring the app
var app = angular.module('studentApp', []);

app.factory('studentFactory',
    function ($http) {
        // creating the object to make http calls
        var webAPIProvider = {};

        // route to the API
        var url = '/api/Student/';

        // adding a method to the http to get the students
        webAPIProvider.getStudents = function() {
            return $http.get(url);
        };

        // add a method for saving a new student
        webAPIProvider.saveStudent = function(studentName) {
            return $http.post(url + studentName);
        }

        // return the object for use
        return webAPIProvider;
    });

app.controller('studentController',
    function($scope, $location, $window, studentFactory) {
        // any code here is going to be executed when the angular app runs

        // this is going to call our get students method
        studentFactory.getStudents()
            // on success assign the scope variable the returned data
            .success(function(data) {
                $scope.students = data;
            });

        $scope.save = function() {
            studentFactory.saveStudent($scope.studentName)
                .success(function(data) {
                    studentFactory.getStudents()
                        // you have to include the promise here too...
                        // leaving this out, the table does not refresh!
                        .success(function(data) {
                            $scope.students = data;
                        });
                });
        };
    });