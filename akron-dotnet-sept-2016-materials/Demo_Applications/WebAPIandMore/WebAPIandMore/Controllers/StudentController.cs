﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIandMore.Data;
using WebAPIandMore.Models;

namespace WebAPIandMore.Controllers
{
    public class StudentController : ApiController
    {
        [HttpGet]
        public List<Student> ThisIsAMethod()
        {
            return Repo.Students;
        }

        public Student GetStudent(int id)
        {
            return Repo.Students.FirstOrDefault(s => s.Id == id);
        }

        [HttpPost]
        public HttpResponseMessage Post(string id)
        {
            var students = Repo.Students;
            students.Add(new Student()
            {
                Id = Repo.Students.Max(s=>s.Id)+1,
                Name = id
            });

            return new HttpResponseMessage(HttpStatusCode.Created);
        }
    }
}
