﻿var myApp = angular.module('teamApp', []);

// factory for creating methods and variables that we can keep around
// also gives us access to our $http object
myApp.factory('teamsFactory', function($http) {
    // initialize an empty object
    var webAPIProvider = {};

    // add a method to get the teams by calling our WebAPI get method
    webAPIProvider.getTeams = function() {
        return $http.get('/api/teams/');
    };

    // initialize an empty array that we can use for filtering
    webAPIProvider.activeCities = [];

    return webAPIProvider;
});

// provides methods and functionality specific to a section of our Angular application
// Not an MVC controller, does NOT do any routing to views on the server side... 
myApp.controller('TeamsController', function ($scope, teamsFactory) {
    // define our method to load the teams using $http.get()
    teamsFactory.getTeams()
        // on success of our Web API call
        .success(function (data) {
            // set teams to the data returned from the call
            $scope.teams = data;

            // if the active cities is not initialized 
            if (teamsFactory.activeCities.length === 0) {
                // loop through all teams and add cities not in list to list
                angular.forEach(data, function(team) {
                    if (teamsFactory.activeCities.indexOf(team.City) === -1) {
                        teamsFactory.activeCities.push(team.City);
                    }
                });
            }
        });

    // define our method to filter teams by the city
    $scope.filterByCity = function($event, city) {
        // get the element executing the event
        var checkbox = $event.target;

        // if our checkbox is checked and the city is not in the array
        if (checkbox.checked && teamsFactory.activeCities.indexOf(city) === -1) {
            // add it
            teamsFactory.activeCities.push(city);
        } else {    // else it's not checked or is in the array
            // if it is in the array (meaning it is no longer checked)
            if (teamsFactory.activeCities.indexOf(city) !== -1) {
                // remove it
                teamsFactory.activeCities.splice(teamsFactory.activeCities.indexOf(city), 1);
            }
        }
    };

    // teamFilter name from ng-repeat
    // function is called for each team in the repeat
    $scope.teamFilter = function (team) {
        // check if the team is in the activeCities array and if not return true
        if (teamsFactory.activeCities.length > 0 && teamsFactory.activeCities.indexOf(team.City) !== -1) {
            return true;
        }

        // do not show
        return false;
    };
    // **************************************************
});

// method being called on the unique key
myApp.filter('unique', function() {
    return function(collection, keyname) {
        var output = []; // array of teams to display
        var keys = [];  // array of strings representing cities

        // loop through the collection
        angular.forEach(collection, function (team) {
            // for each item in the collection return only teams not in output
            var key = team[keyname];
            if (keys.indexOf(key) === -1) {
                output.push(team);
                keys.push(key);
            }
        });

        return output;
    }
});