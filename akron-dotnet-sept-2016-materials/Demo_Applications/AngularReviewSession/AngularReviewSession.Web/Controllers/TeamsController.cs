﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularReviewSession.Data;
using AngularReviewSession.Models;

namespace AngularReviewSession.Web.Controllers
{
    public class TeamsController : ApiController
    {
        public List<Team> Get()
        {
            var repo = new FakeTeamsRepository();
            return repo.GetAll();
        }
    }
}
