﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.Algorithms.Searching
{
    /// <summary>
    /// custom interface for the searches
    /// </summary>
    public interface ITSGSearch
    {
        /// <summary>
        /// method to perform the search
        /// </summary>
        /// <param name="items">array of integers to search</param>
        /// <param name="itemToFind">what item are we looking for?</param>
        /// <param name="output">log of the steps done in the search</param>
        /// <returns>whether the item was found</returns>
        bool Search(int[] items, int itemToFind, StringBuilder output);
    }
}
