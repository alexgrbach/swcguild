﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.Algorithms.Searching
{
    public class BinarySearch : ITSGSearch
    {
        public bool Search(int[] items, int itemToFind, StringBuilder output)
        {
            bool isFound = false;

            int low = 0;
            int high = items.Count();

            while (low <= high)
            {
                int mid = low + (high - low) / 2;

                output.AppendLine($"\tComparing {items[mid]} to {itemToFind}");
                if (items[mid] == itemToFind)
                {
                    isFound = true;
                    break;
                }
                else if (items[mid] < itemToFind)
                {
                    low = mid + 1;
                }
                else
                {
                    high = mid - 1;
                }
            }

            return isFound;
        }
    }
}
