﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.Algorithms.Searching
{
    public class SimpleSearch : ITSGSearch
    {
        public bool Search(int[] items, int itemToFind, StringBuilder output)
        {
            bool isFound = false;
            
            for (int i = 0; i < items.Count(); i++)
            {
                output.AppendLine($"\tComparing {items[i]} to {itemToFind}");

                if (items[i] == itemToFind)
                {
                    isFound = true;
                    break;
                }
            }

            return isFound;
        }
    }
}
