﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DataStructuresAndAlgorithms.Algorithms.Sorting
{
    /// <summary>
    /// custom interface for sorting
    /// </summary>
    public interface ITSGSort
    {
        /// <summary>
        /// method to perform sort
        /// </summary>
        /// <param name="items">items to sort</param>
        void Sort(int[] items, StringBuilder output);
    }
}
