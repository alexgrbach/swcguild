﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.Models
{
    public class Player
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }

        public override string ToString()
        {
            return $"{Number} {Name} - {Position}";
        }
    }
}
