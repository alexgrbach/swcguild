﻿using System;

namespace DataStructuresAndAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            string option = "";
            do
            {
                DrawMenu();
                option = Console.ReadLine();
                Console.WriteLine();

                if (option.ToLower() != "q")
                {
                    ProcessChoice(option);
                }
            } while (option.ToLower() != "q");

            Console.Write("Hit Enter to Quit...");
            Console.ReadLine();
        }

        static void DrawMenu()
        {
            Console.Clear();
            Console.WriteLine("Welcome to Data Structures and Algorithms");
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("Simple Object Implementations");
            Console.WriteLine("1. Stack with Array");
            Console.WriteLine("2. Stack with Linked List");
            Console.WriteLine("3. Queue with Array");
            Console.WriteLine("4. Queue with Linked List");
            Console.WriteLine("5. Single Linked List");
            Console.WriteLine("6. Double Linked List");
            Console.WriteLine("7. Binary Tree");
            Console.WriteLine();
            Console.WriteLine("Searching");
            Console.WriteLine("8. Binary Search");
            Console.WriteLine("9. Simple Search");
            Console.WriteLine();
            Console.WriteLine("Sorting");
            Console.WriteLine("10. Quick Sort");
            Console.WriteLine("11. Selection Sort");
            Console.WriteLine();
            Console.WriteLine("Enter 'q' to quit");
            Console.Write("> ");
        }

        static void ProcessChoice(string Choice)
        {
            switch (Choice.ToLower())
            {
                case "1":
                    SimpleObjectDemos.StackDemoWithArray();
                    break;
                case "2":
                    SimpleObjectDemos.StackDemoWithLinkedList();
                    break;
                case "3":
                    SimpleObjectDemos.QueueDemoWithArray();
                    break;
                case "4":
                    SimpleObjectDemos.QueueDemoWithLinkedList();
                    break;
                case "5":
                    SimpleObjectDemos.LinkedListDemoWithSingleLink();
                    break;
                case "6":
                    SimpleObjectDemos.LinkedListDemoWithDoubleLink();
                    break;
                case "7":
                    SimpleObjectDemos.TreeDemoWithBTree();
                    break;
                case "8":
                    SearchingDemos.BinarySearch();
                    break;
                case "9":
                    SearchingDemos.SimpleSearch();
                    break;
                case "10":
                    SortingDemos.QuickSort();
                    break;
                case "11":
                    SortingDemos.SelectionSort();
                    break;
                default:
                    Console.WriteLine("Invalid or not implemented Option");
                    break;
            }

            Console.ReadLine();
        }
    }
}
