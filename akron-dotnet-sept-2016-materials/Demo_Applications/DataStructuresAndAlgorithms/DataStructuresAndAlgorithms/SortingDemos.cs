﻿using System;
using System.Text;
using DataStructuresAndAlgorithms.Algorithms.Sorting;

namespace DataStructuresAndAlgorithms
{
    public static class SortingDemos
    {
        public static void SelectionSort()
        {
            int[] items = new int[] { 7, 3, 6, 2, 9, 1, 8, 5, 4 };

            Sort(items, new SelectionSort());
        }

        public static void QuickSort()
        {
            int[] items = new int[] { 3, 1, 4, 5, 2 };

            Sort(items, new QuickSort());
        }

        private static void Sort(int[] items, ITSGSort sortType)
        {
            Console.Clear();
            Console.WriteLine(sortType.GetType().Name + " Search Demo");
            Console.WriteLine();

            Console.WriteLine("The Array of items to sort");
            Console.WriteLine(string.Join(" ", items));
            Console.WriteLine();

            StringBuilder sb = new StringBuilder();
            sortType.Sort(items, sb);

            Console.WriteLine("Steps");
            Console.WriteLine(sb.ToString());

            Console.WriteLine();
            Console.WriteLine("The sorted array");
            Console.WriteLine(string.Join(" ", items));
            Console.WriteLine();

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }
    }
}
