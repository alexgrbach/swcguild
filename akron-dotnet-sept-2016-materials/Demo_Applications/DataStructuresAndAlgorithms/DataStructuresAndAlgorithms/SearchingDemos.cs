﻿using System;
using System.Text;
using DataStructuresAndAlgorithms.Algorithms.Searching;

namespace DataStructuresAndAlgorithms
{
    public static class SearchingDemos
    {
        public static void SimpleSearch()
        {
            int[] items = new int[] { 1, 3, 5, 7, 9, 2, 4, 6, 8 };
            int itemToFind = 8;

            SearchDemo(items, itemToFind, new SimpleSearch());
        }

        public static void BinarySearch()
        {
            int[] items = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int itemToFind = 8;
            
            SearchDemo(items, itemToFind, new BinarySearch());
        }

        private static void SearchDemo(int[] items, int itemToFind, ITSGSearch searchType)
        {
            Console.Clear();
            Console.WriteLine(searchType.GetType().Name + " Search Demo");
            Console.WriteLine();

            Console.WriteLine("The Array of items to seach");
            Console.WriteLine(string.Join(" ", items));
            Console.WriteLine();

            StringBuilder sb = new StringBuilder();
            bool isFound = searchType.Search(items, itemToFind, sb);

            Console.WriteLine("Search for " + itemToFind);
            Console.WriteLine(isFound ? "Item was found" : "Item was not found");
            Console.WriteLine();
            Console.WriteLine("Search Steps");
            Console.WriteLine(sb.ToString());

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }
    }
}
