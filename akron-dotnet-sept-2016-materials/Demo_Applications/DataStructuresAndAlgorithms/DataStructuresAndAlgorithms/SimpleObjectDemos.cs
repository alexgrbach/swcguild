﻿using System;
using System.CodeDom;
using System.Linq;
using System.Text;
using DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGLinkedList;
using DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGQueue;
using DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGStack;
using DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGTree;
using DataStructuresAndAlgorithms.Models;

namespace DataStructuresAndAlgorithms
{
    public static class SimpleObjectDemos
    {
        #region Stack Demos
        public static void StackDemoWithArray()
        {
            StackDemo(new TSGArrayStack(9));
        }

        public static void StackDemoWithLinkedList()
        {
            StackDemo(new TSGLinkedListStack());
        }

        private static void StackDemo(ITSGStack myStack)
        {
            Console.Clear();
            Console.WriteLine("Simple Object Demo");
            Console.WriteLine();

            // without the interface implementing the IEnumerable
            // I couldn't iterate/enumerate the collection
            foreach (var player in myStack)
            {
                Console.WriteLine(player);
            }

            myStack.Push(new Player() { Number = 12, Name = "Lindor", Position = 6 });
            myStack.Push(new Player() { Number = 22, Name = "Kipnis", Position = 4 });
            myStack.Push(new Player() { Number = 10, Name = "Gomes", Position = 2 });

            Console.WriteLine("Create a stack with a few items");

            foreach (var player in myStack)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.WriteLine("Pop " + myStack.TopItem);
            Console.WriteLine();

            myStack.Pop();

            Console.WriteLine("Items left on the stack");

            foreach (var player in myStack)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }
        #endregion

        #region Queue Demos

        public static void QueueDemoWithArray()
        {
            QueueDemo(new TSGArrayQueue(4));
        }

        public static void QueueDemoWithLinkedList()
        {
            QueueDemo(new TSGLinkedListQueue());
        }

        private static void QueueDemo(ITSGQueue myQueue)
        {
            Console.Clear();
            Console.WriteLine("Simple Object Demo");
            Console.WriteLine();

            // without the interface implementing the IEnumerable
            // I couldn't iterate/enumerate the collection
            foreach (var player in myQueue)
            {
                Console.WriteLine(player);
            }

            myQueue.Enqueue(new Player() { Number = 12, Name = "Lindor", Position = 6 });
            myQueue.Enqueue(new Player() { Number = 22, Name = "Kipnis", Position = 4 });
            myQueue.Enqueue(new Player() { Number = 10, Name = "Gomes", Position = 2 });

            Console.WriteLine("Create a queue with a few items");

            foreach (var player in myQueue)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.WriteLine("Dequeue " + myQueue.OldestItem);
            Console.WriteLine();

            myQueue.Dequeue();

            Console.WriteLine("Items left on the queue");

            foreach (var player in myQueue)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }

        #endregion

        #region Linked List Demos

        public static void LinkedListDemoWithSingleLink()
        {
            LinkedListDemo(new TSGLinkedListSingle());
        }

        public static void LinkedListDemoWithDoubleLink()
        {
            LinkedListDemo(new TSGLinkedListDouble());

            //TODO: Figure out how to reverse the non-generic linked list
            //Console.WriteLine();
            //Console.WriteLine("Items left in the linked list in reverse");

            //foreach (var player in linkedList.Reverse())
            //{
            //    Console.WriteLine(player);
            //}

            Console.WriteLine();
        }

        private static void LinkedListDemo(ITSGLinkedList linkedList)
        {
            Console.Clear();
            Console.WriteLine("Simple Object Demo");
            Console.WriteLine();

            // without the interface implementing the IEnumerable
            // I couldn't iterate/enumerate the collection
            foreach (var player in linkedList)
            {
                Console.WriteLine(player);
            }

            linkedList.Insert(0, new Player() { Number = 12, Name = "Lindor", Position = 6 });
            linkedList.Insert(1, new Player() { Number = 22, Name = "Kipnis", Position = 4 });
            linkedList.Insert(2, new Player() { Number = 10, Name = "Gomes", Position = 2 });

            Console.WriteLine("Create a linked list with a few items");
            
            foreach (var player in linkedList)
            {
                Console.WriteLine(player);
            }

            linkedList.Insert(2, new Player() { Number = 24, Name = "Miller", Position = 1 });

            Console.WriteLine();
            Console.WriteLine("Insert a new item at position 2");
            
            foreach (var player in linkedList)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.WriteLine("Remove " + linkedList.Find(3));
            Console.WriteLine();

            linkedList.Remove(3);

            Console.WriteLine("Items left in the linked list");

            foreach (var player in linkedList)
            {
                Console.WriteLine(player);
            }

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }

        #endregion

        #region Tree Demos

        public static void TreeDemoWithBTree()
        {
            Console.Clear();
            Console.WriteLine("Simple Object Demo");
            Console.WriteLine();

            int[] ints = new[] { 10, 14, 4, 9, 17, 0, 12, 1, 6, 15, 14, 10, 1 };

            Console.WriteLine("Create Tree from ints");
            Console.WriteLine(string.Join(" ", ints));
            Console.WriteLine();

            TSGBinaryTree tree = new TSGBinaryTree();

            Console.WriteLine("Steps");

            StringBuilder sb;
            foreach (var i in ints)
            {
                sb = new StringBuilder();

                tree.Add(i);

                tree.Draw(sb);
                Console.WriteLine(sb.ToString());
            }

            Console.WriteLine();
            Console.WriteLine("The final tree");
            sb = new StringBuilder();       
            tree.Draw(sb);     
            Console.WriteLine(sb.ToString());

            Console.WriteLine();
            Console.Write("Hit 'enter' to continue...");
        }

        #endregion
    }
}
