﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGQueue
{
    public class TSGLinkedListQueue : ITSGQueue
    {
        public int NumberOfItems { get; private set; }

        public object OldestItem
        {
            get
            {
                if (NumberOfItems > 0)
                {
                    return _oldestNode.Item;
                }

                return null;
            }
        }

        private TSGNode _oldestNode;
        private TSGNode _newestNode;

        public TSGLinkedListQueue()
        {
            _oldestNode = null;
            _newestNode = null;
            NumberOfItems = 0;
        }

        public void Enqueue(object item)
        {
            TSGNode prevNewNode = _newestNode;
            _newestNode = new TSGNode();
            _newestNode.Item = item;
            _newestNode.Next = null;

            if (NumberOfItems == 0)
            {
                _oldestNode = _newestNode;
            }
            else
            {
                prevNewNode.Next = _newestNode;
            }

            NumberOfItems++;
        }

        public object Dequeue()
        {
            object itemToReturn = null;

            if (NumberOfItems > 0)
            {
                itemToReturn = _oldestNode;
                _oldestNode = _oldestNode.Next;
                NumberOfItems--;
            }

            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            TSGNode currentNode;
            TSGNode nextNode = _oldestNode;

            while (nextNode != null)
            {
                currentNode = nextNode;
                nextNode = currentNode.Next;
                yield return currentNode.Item;
            }
        }
    }
}
