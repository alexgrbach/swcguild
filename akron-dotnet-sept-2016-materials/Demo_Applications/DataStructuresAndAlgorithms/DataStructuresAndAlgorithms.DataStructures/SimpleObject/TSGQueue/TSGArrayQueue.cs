﻿using System;
using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGQueue
{
    public class TSGArrayQueue : ITSGQueue
    {
        public int NumberOfItems { get; private set; }
        public object OldestItem { get { return _items[0]; } }

        private object[] _items;
        private int _size;

        public TSGArrayQueue(int size)
        {
            this._size = size;
            _items = new object[size];
            NumberOfItems = 0;
        }

        public void Enqueue(object item)
        {
            if (NumberOfItems != _size)
            {
                _items[NumberOfItems++] = item;
            }
            else
            {
                throw new IndexOutOfRangeException("No room in the queue!");
            }
        }

        public object Dequeue()
        {
            object itemToReturn = null;

            if (NumberOfItems > 0)
            {
                itemToReturn = _items[0];

                NumberOfItems--;

                for (int i = 0; i < NumberOfItems; i++)
                {
                    _items[i] = _items[i + 1];
                }

                _items[NumberOfItems] = null;
            }

            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            for (var i = 0; i < NumberOfItems; i++)
            {
                yield return _items[i];
            }
        }
    }
}
