﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGQueue
{
    /// <summary>
    /// custom queue interface
    /// </summary>
    public interface ITSGQueue : IEnumerable
    {
        /// <summary>
        /// property for number of items actually in the stack
        /// </summary>
        int NumberOfItems { get; }

        /// <summary>
        /// property to return top item on stack without removing
        /// </summary>
        object OldestItem { get; }

        /// <summary>
        /// method to add another item to the queue
        /// </summary>
        /// <param name="item">item to add</param>
        void Enqueue(object item);

        /// <summary>
        /// method to remove the oldest item from the queue
        /// </summary>
        /// <returns>the item removed</returns>
        object Dequeue();
    }
}
