﻿namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject
{
    /// <summary>
    /// class representing a node from a doubly linked list
    /// </summary>
    public class TSGDoubleLinkNode
    {
        /// <summary>
        /// property representing the current item
        /// </summary>
        public object Item { get; set; }

        /// <summary>
        /// property representing the next item in the chain
        /// </summary>
        public TSGDoubleLinkNode Next { get; set; }

        /// <summary>
        /// property representing the previous item in the chain
        /// </summary>
        public TSGDoubleLinkNode Previous { get; set; }
    }
}
