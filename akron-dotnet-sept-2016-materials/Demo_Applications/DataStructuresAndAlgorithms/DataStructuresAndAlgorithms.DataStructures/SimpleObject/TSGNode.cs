﻿using System;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject
{
    /// <summary>
    /// class representing a node in a linked list
    /// </summary>
    public class TSGNode
    {
        /// <summary>
        /// property representing the current item
        /// </summary>
        public object Item { get; set; }

        /// <summary>
        /// property representing the next item in the chain
        /// </summary>
        public TSGNode Next { get; set; }
    }
}
