﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGTree
{
    /// <summary>
    /// custom tree interface
    /// </summary>
    public interface ITSGTree
    {
        /// <summary>
        /// property for number of items actually in the stack
        /// </summary>
        int NumberOfItems { get; }

        /// <summary>
        /// method to add an item to the tree
        /// </summary>
        /// <param name="item">item to add</param>
        void Add(int item);

        /// <summary>
        /// method to draw the tree
        /// </summary>
        /// <param name="output">output container</param>
        void Draw(StringBuilder output);
    }
}
