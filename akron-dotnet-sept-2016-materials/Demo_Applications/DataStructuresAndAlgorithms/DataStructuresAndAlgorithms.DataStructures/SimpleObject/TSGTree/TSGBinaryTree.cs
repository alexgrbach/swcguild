﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGTree
{
    public class TSGBinaryTree : ITSGTree
    {
        public int NumberOfItems { get; private set; }

        private TSGDoubleLinkNode _topNode;

        public void Add(int item)
        {
            if (_topNode == null)
            {
                _topNode = Create(item);
            }
            else
            {
                TSGDoubleLinkNode prevParent = null;
                TSGDoubleLinkNode node = _topNode;
                bool isLeft = false;
                while (node != null)
                {
                    prevParent = node;
                    if (item < (int)node.Item)
                    {
                        node = node.Previous;
                        isLeft = true;
                    }
                    else
                    {
                        node = node.Next;
                        isLeft = false;
                    }
                }

                if (isLeft)
                {
                    prevParent.Previous = Create(item);
                }
                else
                {
                    prevParent.Next = Create(item);
                }
            }
            NumberOfItems++;
        }

        public void Draw(StringBuilder output)
        {
            Write(_topNode, output);
        }

        private void Write(TSGDoubleLinkNode node, StringBuilder output)
        {
            if (node != null)
            {
                Write(node.Previous, output);
                output.Append($"{node.Item,2} ");
                Write(node.Next, output);
            }
        }

        private TSGDoubleLinkNode Create(int item)
        {
            TSGDoubleLinkNode node = new TSGDoubleLinkNode();
            node.Item = item;
            node.Next = null;
            node.Previous = null;

            return node;
        }
    }
}
