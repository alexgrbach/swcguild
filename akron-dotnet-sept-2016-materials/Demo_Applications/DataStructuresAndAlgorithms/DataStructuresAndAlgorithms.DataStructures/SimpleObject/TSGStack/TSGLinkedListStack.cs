﻿using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGStack
{
    public class TSGLinkedListStack : ITSGStack
    {
        public int NumberOfItems { get; private set; }

        public object TopItem
        {
            get
            {
                if (NumberOfItems > 0)
                {
                    return _topNode.Item;
                }

                return null;
            }
        }

        private TSGNode _topNode;

        public TSGLinkedListStack()
        {
            _topNode = null;
            NumberOfItems = 0;
        }

        public void Push(object item)
        {
            TSGNode prevItem = _topNode;
            _topNode = new TSGNode();
            _topNode.Item = item;
            _topNode.Next = prevItem;
            NumberOfItems++;
        }

        public object Pop()
        {
            object itemToReturn = null;

            if (NumberOfItems > 0)
            {
                itemToReturn = _topNode.Item;
                _topNode = _topNode.Next;
                NumberOfItems--;
            }

            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            TSGNode currentNode;
            TSGNode nextNode = _topNode;

            while (nextNode != null)
            {
                currentNode = nextNode;
                nextNode = currentNode.Next;
                yield return currentNode.Item;
            }
        }
    }
}
