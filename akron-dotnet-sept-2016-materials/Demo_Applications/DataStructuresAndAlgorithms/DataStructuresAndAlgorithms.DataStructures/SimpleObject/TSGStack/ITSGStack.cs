﻿using System;
using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGStack
{
    /// <summary>
    /// custom stack interface
    /// </summary>
    public interface ITSGStack : IEnumerable
    {
        /// <summary>
        /// property for number of items actually in the stack
        /// </summary>
        int NumberOfItems { get; }

        /// <summary>
        /// property to return top item on stack without removing
        /// </summary>
        object TopItem { get; }

        /// <summary>
        /// method to add another item to the stack
        /// </summary>
        /// <param name="item">item to add</param>
        void Push(object item);

        /// <summary>
        /// method to remove the top item from the stack
        /// </summary>
        /// <returns>the item removed</returns>
        object Pop();
    }
}
