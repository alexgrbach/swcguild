﻿using System;
using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGStack
{
    public class TSGArrayStack : ITSGStack
    {
        public int NumberOfItems { get; private set; }
        public object TopItem { get { return _items[NumberOfItems - 1]; } }

        private object[] _items;
        private int _size;

        public TSGArrayStack(int size)
        {
            this._size = size;
            _items = new object[size];
            NumberOfItems = 0;
        }

        public void Push(object item)
        {
            if (NumberOfItems != _size)
            {
                _items[NumberOfItems++] = item;
            }
            else
            {
                throw new IndexOutOfRangeException("No room on the stack!");
            }
        }

        public object Pop()
        {
            object itemToReturn = null;

            if (NumberOfItems > 0)
            {
                itemToReturn = _items[--NumberOfItems];
                _items[NumberOfItems] = null;
            }
            
            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            // REVERSE!!! 
            // It's a stack... the item in position 0 is on the bottom...
            for (var i = NumberOfItems - 1; i >= 0; i--)
            {
                yield return _items[i];
            }
        }
    }
}
