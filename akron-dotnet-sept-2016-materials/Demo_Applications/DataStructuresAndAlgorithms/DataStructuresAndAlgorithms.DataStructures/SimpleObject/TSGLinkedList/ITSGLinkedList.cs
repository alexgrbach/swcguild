﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGLinkedList
{
    /// <summary>
    /// custom linked list interface
    /// </summary>
    public interface ITSGLinkedList : IEnumerable
    {
        /// <summary>
        /// property for number of items actually in the linked list
        /// </summary>
        int NumberOfItems { get; }

        /// <summary>
        /// property to return first item on linked list without removing
        /// </summary>
        object FirstItem { get; }

        /// <summary>
        /// method to add another item to the linked list
        /// </summary>
        /// <param name="position">where in the linked list to insert</param>
        /// <param name="item">item to add</param>
        void Insert(int position, object item);

        /// <summary>
        /// method to find item in the linked list
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        object Find(int position);

        /// <summary>
        /// method to remove the oldest item from the linked list
        /// </summary>
        /// <param name="position">where in the linked list to remove</param>
        /// <returns>the item removed</returns>
        object Remove(int position);
    }
}
