﻿using System;
using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGLinkedList
{
    public class TSGLinkedListDouble : ITSGLinkedList
    {
        public int NumberOfItems { get; private set; }

        public object FirstItem
        {
            get
            {
                if (NumberOfItems > 0)
                {
                    return _firstNode.Item;
                }

                return null;
            }
        }

        public object LastItem
        {
            get
            {
                if (NumberOfItems > 0)
                {
                    return _lastNode.Item;
                }

                return null;
            }
        }

        private TSGDoubleLinkNode _firstNode;
        private TSGDoubleLinkNode _lastNode;

        public TSGLinkedListDouble()
        {
            NumberOfItems = 0;
            _firstNode = null;
            _lastNode = null;
        }

        public void Insert(int position, object item)
        {
            if (position <= NumberOfItems)
            {
                TSGDoubleLinkNode newNode = new TSGDoubleLinkNode();
                newNode.Item = item;

                TSGDoubleLinkNode prevNode = null;
                TSGDoubleLinkNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    prevNode = currentNode;
                    currentNode = currentNode.Next;
                }

                if (prevNode != null) 
                {
                    prevNode.Next = newNode;
                    newNode.Previous = prevNode;

                    if (currentNode != null)
                    {
                        currentNode.Previous = newNode;
                    }
                }
                else    // position == 0
                {
                    _firstNode = newNode;
                    _lastNode = newNode;
                }

                newNode.Next = currentNode;

                NumberOfItems++;
            }
            else
            {
                throw new IndexOutOfRangeException("invalid index for current list!");
            }
        }

        public object Find(int position)
        {
            object itemToReturn = null;

            if (position < NumberOfItems)
            {
                TSGDoubleLinkNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    currentNode = currentNode.Next;
                }

                itemToReturn = currentNode.Item;
            }

            return itemToReturn;
        }

        public object Remove(int position)
        {
            object itemToReturn = null;

            if (position < NumberOfItems)
            {
                TSGDoubleLinkNode prevNode = null;
                TSGDoubleLinkNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    prevNode = currentNode;
                    currentNode = currentNode.Next;
                }

                if (prevNode != null)
                {
                    prevNode.Next = currentNode.Next;

                    if (currentNode.Next != null)
                    {
                        currentNode.Next.Previous = prevNode;
                    }
                    else
                    {
                        _lastNode = prevNode;
                    }
                }
                else    // position == 0
                {
                    _firstNode = currentNode.Next;
                }

                itemToReturn = currentNode.Item;

                NumberOfItems--;
            }

            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            TSGDoubleLinkNode currentNode;
            TSGDoubleLinkNode nextNode = _firstNode;

            while (nextNode != null)
            {
                currentNode = nextNode;
                nextNode = currentNode.Next;
                yield return currentNode.Item;
            }
        }
    }
}
