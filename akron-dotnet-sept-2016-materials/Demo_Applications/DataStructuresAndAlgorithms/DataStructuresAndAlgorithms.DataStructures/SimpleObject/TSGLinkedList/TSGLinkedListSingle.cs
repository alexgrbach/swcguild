﻿using System;
using System.Collections;

namespace DataStructuresAndAlgorithms.DataStructures.SimpleObject.TSGLinkedList
{
    public class TSGLinkedListSingle : ITSGLinkedList
    {
        public int NumberOfItems { get; private set; }

        public object FirstItem
        {
            get
            {
                if (NumberOfItems > 0)
                {
                    return _firstNode.Item;
                }

                return null;
            }
        }

        private TSGNode _firstNode;

        public TSGLinkedListSingle()
        {
            NumberOfItems = 0;
            _firstNode = null;
        }

        public void Insert(int position, object item)
        {
            if (position <= NumberOfItems)
            {
                TSGNode newNode = new TSGNode();
                newNode.Item = item;

                TSGNode prevNode = null;
                TSGNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    prevNode = currentNode;
                    currentNode = currentNode.Next;
                }

                if (prevNode != null)
                {
                    prevNode.Next = newNode;
                }
                else // position == 0
                {
                    _firstNode = newNode;
                }

                newNode.Next = currentNode;

                NumberOfItems++;
            }
            else
            {
                throw new IndexOutOfRangeException("invalid index for current list!");
            }
        }

        public object Find(int position)
        {
            object itemToReturn = null;

            if (position < NumberOfItems)
            {
                TSGNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    currentNode = currentNode.Next;
                }

                itemToReturn = currentNode.Item;
            }

            return itemToReturn;
        }

        public object Remove(int position)
        {
            object itemToReturn = null;

            if (position < NumberOfItems)
            {
                TSGNode prevNode = null;
                TSGNode currentNode = _firstNode;

                for (var i = 0; i < position; i++)
                {
                    prevNode = currentNode;
                    currentNode = currentNode.Next;
                }

                if (prevNode != null)
                {
                    prevNode.Next = currentNode.Next;
                }
                else    //position == 0
                {
                    _firstNode = currentNode.Next;
                }

                itemToReturn = currentNode.Item;

                NumberOfItems--;
            }

            return itemToReturn;
        }

        public IEnumerator GetEnumerator()
        {
            TSGNode currentNode;
            TSGNode nextNode = _firstNode;

            while (nextNode != null)
            {
                currentNode = nextNode;
                nextNode = currentNode.Next;
                yield return currentNode.Item;
            }
        }
    }
}
