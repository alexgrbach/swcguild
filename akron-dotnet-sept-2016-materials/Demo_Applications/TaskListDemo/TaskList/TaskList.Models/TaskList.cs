﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TaskList.Models
{
    public class TaskList
    {
        public int TaskListId { get; set; }
        public string TaskListName { get; set; }

        public List<Task> Tasks { get; set; }
    }
}
