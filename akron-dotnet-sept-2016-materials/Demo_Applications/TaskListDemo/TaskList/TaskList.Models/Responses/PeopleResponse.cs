﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskList.Models.Responses
{
    public class PeopleResponse
    {
        public List<Person> People { get; set; }
        public bool Success { get; set; }
    }
}
