﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TaskList.Models.Responses
{
    public class TasksResponse
    {
        public List<Task> Tasks { get; set; }
        public bool Success { get; set; }
    }
}
