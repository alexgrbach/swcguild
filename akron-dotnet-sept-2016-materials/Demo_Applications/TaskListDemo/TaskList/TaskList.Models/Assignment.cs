﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskList.Models
{
    public class Assignment
    {
        public string Role { get; set; }

        public Person AssignedPerson { get; set; }
        public Task AssignedTask { get; set; }
    }
}
