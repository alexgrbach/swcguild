﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskList.Models
{
    public class Task
    {
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public DateTime? DueDate { get; set; }
        public int? Priority { get; set; }

        public TaskList OnList { get; set; }
        public List<Assignment> Assignments { get; set; }
    }
}
