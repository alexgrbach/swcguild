﻿Solution Structure (Projects)
--------------------------------
TaskList.BLL - layer for calculations and any logic required in the application
TaskList.Data - gets and sets data from where the storage is located.
TaskList.Models - class definitions for the data that will be passed between layers
TaskList.Web - Web front end project that the user will actually see

Demonstration Pieces
-----------------------
Angular Filtering, WebAPI
	The Tasks is set up to demonstrate the topics above. The Tasks have been organized 
into a single view by which they are populated via a webAPI controller. WebAPI Controllers
differ from MVC controllers in that they generally focus on a single piece of data and the
CRUD (POST, GET, PUT, DELETE) operations around that data. In many cases you may find that
the controller methods are simply called the verb they represent. This allows default 
routing to assist. 
	The Angular piece of the solution is then where the data is being populated onto the 
view. The View itself sets up the table for the data but MVC does NOT load any data from
the database. Instead once the page is loaded the Javascript taskAJAX.js will execute it's 
method to call the WebAPI controller and get the data from the database and then use that 
data to fill the HTML table. This Angular piece includes a controller (Angular controller)
that makes the Web API call by utilizing the Factory within Angular which creates the $http
object and makes that available. The http object is then used to call the Web API method 
and then the data is populated to the table.
	Upon being populated to the table you will note that the View contains a text box for
filtering. This text box coupled with the filter: attribute on the ng-repeat allow filtering
of the data within the table. Now if we didn't care which attribute of the data we filtered
by, meaning we will just search all properties of the data we could leave off the {...} and 
just provide the name of the filter. In our case we are providing the TaskName attribute as
the field we wish to restrict filtering. Hence, only that field will be checked for the 
value typed into the filter text box. 

Databases
	The application is written with a backend database. If you check the folder one level up
from the solution you will find the SQL script used to generate the database. This script is
an export from SQL Server to make the database easy to load onto a new machine. It can be run
by simply loading it into SQL Server Management Studio (SSMS). 
	To connect to the database, I have used ADO.NET for connection, commands and reading the 
data. This structure will show you the individual steps to executing queries.
	- Create the connection 
	- create the command
	- execute the command and get a DataReader
	- read while there is data to be read

	Executing Insert, Update or Delete is very similar only there is no reader. For the Select
execution you can refer to Tasks. For Insert you can refer to People. 

Modal, Partial View
	For the Modal and Partial View, I first structured this to have the modal be inside of the 
partial view. This is a good practice for creating them as it allows you to separate the HTML
of the Modal into a separate file. This partial view also includes the button as that is 
really part of the modal and can be in the same file thus giving more organization. So, in 
essense the partial view is a way to break out a portion of the page so that I can have cleaner 
code in my main view. 
	The modal is then the bootstrap modal. Due to the naming conventions and the attributes on
the HTML tags the actual showing and hiding of the modal is done via bootstrap without any 
javascript. If you want to clear the fields of the modal before display you will need a small 
javascript tied to the button click. This is the easiest way to have that modal display. 
	In terms of the add functionality I have here, I decided rather than add more complexity
to the page, I simplified and made the add MVC. You could plug in more javascript and a web
API controller to do this via AJAX if you wished. 