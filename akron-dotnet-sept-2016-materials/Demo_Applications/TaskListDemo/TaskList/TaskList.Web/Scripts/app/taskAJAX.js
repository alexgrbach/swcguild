﻿// create an Angular application object
// notice the empty array meaning no extras, just core Angular
var myApp = angular.module('tasksApp', []);

// The factory will provide a way to make those AJAX calls we like
// AJAX is going to call our Web API
myApp.factory('tasksFactory',
    function ($http) {
        // create a new object
        var webAPIProvider = {};

        // url of our WebAPI Controller
        var url = '/api/tasksapi/';

        // add a method to get all the tasks
        webAPIProvider.getTasks = function () {
            return $http.get(url);
        };

        // return our wrapped API object
        return webAPIProvider;
    });

// controller for the get list.
myApp.controller('TasksController',
    function ($scope, tasksFactory) {
        // call the get tasks method as defined above. 
        tasksFactory.getTasks()
            .success(function (data) {
                // if the call succeeds set the scope 
                // variable called tasks.
                $scope.tasks = data;
            })
            .error(function (data, status) {
                alert('ERROR! status: ' + status);
            });
    });
