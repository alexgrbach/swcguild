﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using TaskList.BLL;

namespace TaskList.Web.Controllers
{
    public class TasksAPIController : ApiController
    {
        // note the object being returned and not ActionResult
        // Web API controllers are going to return JSON, not views or anything else.
        public List<TaskList.Models.Task> Get()
        {
            // use the BLL to execute the functionality we want
            var ops = new TaskOperations();
            var response = ops.GetAllTasks();

            // return the collection we wanted. 
            return response.Tasks;
        }
    }
}
