﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskList.BLL;
using TaskList.Models;

namespace TaskList.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tasks()
        {
            return View();
        }

        public ActionResult People()
        {
            var ops = new PeopleOperations();
            var response = ops.GetAllPeople();

            return View(response.People);
        }

        [HttpPost]
        public ActionResult AddPerson(Person newPerson)
        {
            var ops = new PeopleOperations();
            var response = ops.AddPerson(newPerson);

            return View("People", response.People);
        }
    }
}