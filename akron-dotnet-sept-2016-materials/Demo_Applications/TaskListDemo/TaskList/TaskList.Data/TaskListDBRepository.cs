﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TaskList.Data
{
    public class TaskListDBRepository
    {
        // The connection string that I have defined in the web.config of the Web project
        private readonly string _connectionString =
            ConfigurationManager.ConnectionStrings["TaskList"].ConnectionString;

        /// <summary>
        /// method to return all the tasks in the system. 
        /// </summary>
        /// <returns>a list of task objects</returns>
        public List<TaskList.Models.Task> GetAllTasks()
        {
            List<TaskList.Models.Task> tasks = new List<TaskList.Models.Task>();

            // connecting to a database involves these steps:

            // 1. creating a connection
            using (var cn = new SqlConnection(_connectionString))
            {
                // 2. creating a command object
                var cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = @"SELECT TaskId, TaskName, DueDate, Priority
                                    FROM Tasks";

                // 3. open the connection
                cn.Open();

                // 4. execute the command
                using (var dr = cmd.ExecuteReader())
                {
                    // 5. read the rows that are returned. 
                    while (dr.Read())
                    {
                        // convert the row to C# object
                        TaskList.Models.Task newTask = ConvertReaderToTask(dr);

                        // add to the list
                        tasks.Add(newTask);
                    }
                }
            }

            // return the list
            return tasks;
        }

        /// <summary>
        /// This method makes the transititon from database table to object
        /// </summary>
        /// <param name="dr">data reader object containing database row</param>
        /// <returns>object translation from table data</returns>
        private TaskList.Models.Task ConvertReaderToTask(SqlDataReader dr)
        {
            // create a new object. 
            // notice I used fully qualified naming, that is because I ran into a naming conflict
            TaskList.Models.Task newTask = new TaskList.Models.Task();

            // map the columns in the database to properties on the object
            newTask.TaskId = (int) dr["TaskId"];
            newTask.TaskName = dr["TaskName"].ToString();

            // check those properties that might be null
            if (dr["DueDate"] != DBNull.Value)
            {
                newTask.DueDate = DateTime.Parse(dr["DueDate"].ToString());
            }

            if (dr["Priority"] != DBNull.Value)
            {
                newTask.Priority = (int) dr["Priority"];
            }

            // return the object. 
            return newTask;
        }
    }
}
