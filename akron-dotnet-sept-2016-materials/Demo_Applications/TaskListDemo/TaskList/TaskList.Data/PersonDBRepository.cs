﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskList.Models;

namespace TaskList.Data
{
    public class PersonDBRepository
    {
        // The connection string that I have defined in the web.config of the Web project
        private readonly string _connectionString =
            ConfigurationManager.ConnectionStrings["TaskList"].ConnectionString;

        /// <summary>
        /// method to return all the people in the system
        /// </summary>
        /// <returns>a list of people objects</returns>
        public List<Person> GetAllPeople()
        {
            List<Person> people = new List<Person>();

            // connecting to a database involves these steps:

            // 1. creating a connection
            using (var cn = new SqlConnection(_connectionString))
            {
                // 2. creating a command object
                var cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = @"SELECT PersonId, FirstName, LastName
                                    FROM People";

                // 3. open the connection
                cn.Open();

                // 4. execute the command
                using (var dr = cmd.ExecuteReader())
                {
                    // 5. read the rows that are returned. 
                    while (dr.Read())
                    {
                        // convert the row to C# object
                        TaskList.Models.Person newPerson = ConvertReaderToPerson(dr);

                        // add to the list
                        people.Add(newPerson);
                    }
                }
            }

            // return the list
            return people;
        }

        /// <summary>
        /// This method makes the transititon from database table to object
        /// </summary>
        /// <param name="dr">data reader object containing database row</param>
        /// <returns>object translation from table data</returns>
        private TaskList.Models.Person ConvertReaderToPerson(SqlDataReader dr)
        {
            // create a new object. 
            // notice I used fully qualified naming, that is because I ran into a naming conflict
            TaskList.Models.Person newPerson = new TaskList.Models.Person
            {
                PersonId = (int) dr["PersonId"],
                FirstName = dr["FirstName"].ToString(),
                LastName = dr["LastName"].ToString()
            };

            // map the columns in the database to properties on the object

            // return the object. 
            return newPerson;
        }

        public void AddPerson(Person newPerson)
        {
            // 1. creating a connection
            using (var cn = new SqlConnection(_connectionString))
            {
                // 2. creating a command object
                var cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.CommandText = @"INSERT INTO People(FirstName, LastName)
                                    VALUES (@FirstName, @LastName)";

                // 3. set the parameters
                // always parameterize the query
                cmd.Parameters.AddWithValue("@FirstName", newPerson.FirstName);
                cmd.Parameters.AddWithValue("@LastName", newPerson.LastName);

                // 4. open the connection
                cn.Open();

                // 5. execute the non-query
                // it is a non-query since we aren't looking to return any data
                cmd.ExecuteNonQuery();
            }
        }
    }
}
