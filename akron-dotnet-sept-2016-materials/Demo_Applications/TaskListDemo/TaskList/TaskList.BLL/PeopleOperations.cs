﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskList.Data;
using TaskList.Models;
using TaskList.Models.Responses;

namespace TaskList.BLL
{
    public class PeopleOperations
    {
        public PeopleResponse GetAllPeople()
        {
            // create the repository object
            PersonDBRepository repo = new PersonDBRepository();

            // create a new response object
            // remember the response objects? no reason we can't use them again... 
            PeopleResponse response = new PeopleResponse();

            // set the properties of the response
            response.People = repo.GetAllPeople();

            // we are assuming success here
            response.Success = true;

            // return the object
            return response;
        }

        public PeopleResponse AddPerson(Person newPerson)
        {
            // create the repository object
            PersonDBRepository repo = new PersonDBRepository();

            // create a new response object
            // remember the response objects? no reason we can't use them again... 
            PeopleResponse response = new PeopleResponse();

            //add the new person to the database
            repo.AddPerson(newPerson);

            // set the properties of the response
            response.People = repo.GetAllPeople();

            // we are assuming success here
            response.Success = true;

            // return the object
            return response;
        }
    }
}
