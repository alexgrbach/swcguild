﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskList.Data;
using TaskList.Models.Responses;

namespace TaskList.BLL
{
    public class TaskOperations
    {
        public TasksResponse GetAllTasks()
        {
            // create the repository object
            TaskListDBRepository repo = new TaskListDBRepository();

            // create a new response object
            // remember the response objects? no reason we can't use them again... 
            TasksResponse response = new TasksResponse();

            // set the properties of the response
            response.Tasks = repo.GetAllTasks();

            // we are assuming success here
            response.Success = true;

            // return the object
            return response;
        }
    }
}
