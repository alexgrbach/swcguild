--JOIN Statements
SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
LEFT JOIN Employees
	ON Orders.EmployeeID = Employees.EmployeeID
WHERE Orders.EmployeeID IS NULL


SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
RIGHT JOIN Employees
	ON Orders.EmployeeID = Employees.EmployeeID

SELECT OrderID, CustomerID, Orders.EmployeeID, LastName, FirstName
FROM Orders
FULL OUTER JOIN Employees
	ON Orders.EmployeeID = Employees.EmployeeID