USE master 
GO 

IF EXISTS( SELECT * FROM sys.sysdatabases WHERE [name] = 'MovieCatalogue') 
DROP DATABASE MovieCatalogue 
GO 

CREATE DATABASE MovieCatalogue 
GO 

USE MovieCatalogue 
GO 
CREATE TABLE Movie 
( 
	MovieID INT IDENTITY(1,1) PRIMARY KEY, 
	Title VARCHAR(30) NOT NULL, 
	Runtime INT NULL, 
	Rating VARCHAR(5) NULL,
	ReleaseDate DATE NULL
) 
GO 

INSERT INTO Movie VALUES 
	('Major League', 107,'R', '4-7-1989'), 
	('A League of Their Own', NULL,'PG', '7-1-1992')
	
INSERT INTO Movie (Title, Rating)
	VALUES ('Field of Dreams', 'PG'),
	('The Sandlot', 'PG')
	
INSERT INTO Movie (Title, Runtime, Rating)
	VALUES ('The Lion King', 89, 'G'),
	('Beauty and the Beast', 84, 'G'),
	('Aladdin', 90, 'G')
	
-- more date if you need it...
--	INSERT INTO Movie (Title, Runtime, Rating) VALUES 
--	('A-List Explorers', 96,'PG-13'), 
--	('Bonker Bonzo', 75,'G'), 
--	('Chumps to Champs', 75,'PG-13'), 
--	('Dare or Die', 110,'R'), 
--	('EeeeGhads', 88,'G')