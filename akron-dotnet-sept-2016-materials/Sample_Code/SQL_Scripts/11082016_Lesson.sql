--parameterizing the query
DECLARE @CustomerID nchar(5)
SET @CustomerID = 'BOTTM'

SELECT * 
FROM Customers
WHERE CustomerID = @CustomerID

SELECT *
FROM Orders
WHERE CustomerID = @CustomerID

DECLARE @MinOrderDate datetime
DECLARE @MaxOrderDate datetime

SET @MinOrderDate = '1/1/1997'
SET @MaxOrderDate = '12/31/1997'

SELECT *
FROM Orders
WHERE CustomerID = @CustomerID
	AND OrderDate BETWEEN @MinOrderDate AND @MaxOrderDate

--selecting nothing
SELECT 'Nothing'
-- demonstrated selecting a single value not from a table.

-- ORDER BY DEMOS
SELECT LastName, FirstName
FROM Employees
ORDER BY LastName

SELECT LastName, FirstName
FROM Employees
ORDER BY LastName DESC

SELECT emp.LastName, emp.FirstName, mgr.LastName
FROM Employees emp
	LEFT JOIN Employees mgr						--don't forget about aliases
		ON mgr.EmployeeID = emp.ReportsTo
WHERE emp.ReportsTo IS NOT NULL					--can filter out nulls
ORDER BY emp.HireDate DESC, emp.LastName DESC	--can sort by multiple columns

-- sorting with nulls
SELECT FirstName, LastName, ReportsTo
FROM Employees
ORDER BY ReportsTo DESC
--null at the top in ASC
--null at the bottom in DESC

--Joining to a bridge table
SELECT o.OrderID, o.OrderDate
		, od.Discount, od.Quantity, od.UnitPrice
		, p.ProductName
FROM Orders o
	INNER JOIN [Order Details] od
		ON o.OrderID = od.OrderID
	INNER JOIN Products p
		ON od.ProductID = p.ProductID

--EXPRESSIONS
SELECT o.OrderID
		, CONVERT(NVARCHAR, o.OrderDate, 1)			--use convert to change
		, CONVERT(NVARCHAR, o.OrderDate, 101)		-- how the data is formatted
		, CONVERT(NVARCHAR, o.OrderDate, 107)
		, CONVERT(NVARCHAR, o.OrderDate, 110)
		, od.Discount, od.Quantity, od.UnitPrice
		--Convert again will change formatting
		--Round make it round to certain decimal place
		-- Hey! I can do math!
		-- and you can alias the field
		, CONVERT(MONEY, ROUND(((1 - od.Discount) * od.UnitPrice * Quantity),2)) AS Total
		--AND you can alias a field without math
		, p.ProductName AS 'Product Name'
FROM Orders o
	INNER JOIN [Order Details] od
		ON o.OrderID = od.OrderID
	INNER JOIN Products p
		ON od.ProductID = p.ProductID
ORDER BY Total	--order by a calculated field

-- AGGREGATES
SELECT o.OrderID, o.OrderDate
		, COUNT(*) AS 'Count of Different Products'
		, SUM(od.Quantity) AS 'TOTAL ITEMS'
FROM Orders o
	INNER JOIN [Order Details] od
		ON o.OrderID = od.OrderID
GROUP BY o.OrderID, o.OrderDate

--Check the aggregates
SELECT o.OrderID, o.OrderDate
		, od.Discount, od.Quantity, od.UnitPrice
		, p.ProductName
FROM Orders o
	INNER JOIN [Order Details] od
		ON o.OrderID = od.OrderID
	INNER JOIN Products p
		ON od.ProductID = p.ProductID
WHERE o.OrderID = 10248

--Filter by an aggregate
SELECT o.OrderID, o.OrderDate
		, COUNT(*) AS 'Count of Different Products'
		, SUM(od.Quantity) AS 'TOTAL ITEMS'
FROM Orders o
	INNER JOIN [Order Details] od
		ON o.OrderID = od.OrderID
GROUP BY o.OrderID, o.OrderDate
HAVING COUNT(*) > 3
