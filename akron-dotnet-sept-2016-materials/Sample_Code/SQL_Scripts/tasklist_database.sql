USE [master]
GO
/****** Object:  Database [TaskListDemo]    Script Date: 11/11/16 10:28:08 AM ******/
CREATE DATABASE [TaskListDemo]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TaskListDemo', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TaskListDemo.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TaskListDemo_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TaskListDemo_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TaskListDemo] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TaskListDemo].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TaskListDemo] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TaskListDemo] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TaskListDemo] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TaskListDemo] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TaskListDemo] SET ARITHABORT OFF 
GO
ALTER DATABASE [TaskListDemo] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TaskListDemo] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TaskListDemo] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TaskListDemo] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TaskListDemo] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TaskListDemo] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TaskListDemo] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TaskListDemo] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TaskListDemo] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TaskListDemo] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TaskListDemo] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TaskListDemo] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TaskListDemo] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TaskListDemo] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TaskListDemo] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TaskListDemo] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TaskListDemo] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TaskListDemo] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TaskListDemo] SET  MULTI_USER 
GO
ALTER DATABASE [TaskListDemo] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TaskListDemo] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TaskListDemo] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TaskListDemo] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TaskListDemo] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TaskListDemo]
GO
/****** Object:  Table [dbo].[People]    Script Date: 11/11/16 10:28:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[People](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
 CONSTRAINT [PK_People] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaskLists]    Script Date: 11/11/16 10:28:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskLists](
	[TaskListId] [int] IDENTITY(1,1) NOT NULL,
	[TaskListName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_TaskLists] PRIMARY KEY CLUSTERED 
(
	[TaskListId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 11/11/16 10:28:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[TaskName] [nvarchar](100) NOT NULL,
	[DueDate] [datetime] NULL,
	[Priority] [int] NULL,
	[TaskListId] [int] NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TasksAssigned]    Script Date: 11/11/16 10:28:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TasksAssigned](
	[TaskId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[Role] [nvarchar](100) NULL,
 CONSTRAINT [PK_TasksAssigned] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC,
	[PersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[People] ON 

INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (1, N'Victor', N'Pudelski')
INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (2, N'Sarah', N'Dutkiewicz')
INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (3, N'Pat ', N'Toner')
INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (4, N'Dave ', N'Balzer')
INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (5, N'Eric', N'Ward')
INSERT [dbo].[People] ([PersonId], [FirstName], [LastName]) VALUES (6, N'Morgan ', N'Willis')
SET IDENTITY_INSERT [dbo].[People] OFF
SET IDENTITY_INSERT [dbo].[TaskLists] ON 

INSERT [dbo].[TaskLists] ([TaskListId], [TaskListName]) VALUES (1, N'Curriculum')
SET IDENTITY_INSERT [dbo].[TaskLists] OFF
SET IDENTITY_INSERT [dbo].[Tasks] ON 

INSERT [dbo].[Tasks] ([TaskId], [TaskName], [DueDate], [Priority], [TaskListId]) VALUES (1, N'.NET Core ', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Tasks] ([TaskId], [TaskName], [DueDate], [Priority], [TaskListId]) VALUES (2, N'Angular 2', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Tasks] ([TaskId], [TaskName], [DueDate], [Priority], [TaskListId]) VALUES (3, N'React', CAST(N'2016-07-01 00:00:00.000' AS DateTime), NULL, 1)
INSERT [dbo].[Tasks] ([TaskId], [TaskName], [DueDate], [Priority], [TaskListId]) VALUES (4, N'Spring Boot', CAST(N'2017-01-01 00:00:00.000' AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[Tasks] OFF
ALTER TABLE [dbo].[Tasks]  WITH CHECK ADD  CONSTRAINT [FK_Tasks_TaskLists] FOREIGN KEY([TaskListId])
REFERENCES [dbo].[TaskLists] ([TaskListId])
GO
ALTER TABLE [dbo].[Tasks] CHECK CONSTRAINT [FK_Tasks_TaskLists]
GO
ALTER TABLE [dbo].[TasksAssigned]  WITH CHECK ADD  CONSTRAINT [FK_TasksAssigned_People] FOREIGN KEY([PersonId])
REFERENCES [dbo].[People] ([PersonId])
GO
ALTER TABLE [dbo].[TasksAssigned] CHECK CONSTRAINT [FK_TasksAssigned_People]
GO
ALTER TABLE [dbo].[TasksAssigned]  WITH CHECK ADD  CONSTRAINT [FK_TasksAssigned_Tasks] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Tasks] ([TaskId])
GO
ALTER TABLE [dbo].[TasksAssigned] CHECK CONSTRAINT [FK_TasksAssigned_Tasks]
GO
USE [master]
GO
ALTER DATABASE [TaskListDemo] SET  READ_WRITE 
GO
