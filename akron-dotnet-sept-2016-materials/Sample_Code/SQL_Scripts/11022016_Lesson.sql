--switches the database to be Northwind
USE Northwind
GO

--Show me the customers
SELECT *
FROM [Customers]	--[] are optional unless reseved work
					-- or there are spaces

/*
anything inbetween the /* and */
is a comment
*/

-- filter by a single country
SELECT *
FROM Customers
WHERE Country = 'USA'

-- filter out a single country
SELECT *
FROM Customers
WHERE Country != 'USA'

-- do logic
SELECT * 
FROM Customers
WHERE Country = 'USA' AND Region = 'OR'

-- select from list
SELECT * 
FROM Customers
WHERE Country IN ('USA', 'UK')

-- select not from list
SELECT * 
FROM Customers
WHERE Country NOT IN ('USA', 'UK')

-- LIKE statement
SELECT *
FROM Customers
WHERE CompanyName LIKE 'A[^N]%'

SELECT *
FROM Customers
WHERE CompanyName LIKE 'A%'

-- T not the third character
SELECT *
FROM Customers
WHERE CompanyName LIKE 'A_[^T]%'

-- return just the customer ID and Company name
SELECT CustomerID, CompanyName
FROM Customers