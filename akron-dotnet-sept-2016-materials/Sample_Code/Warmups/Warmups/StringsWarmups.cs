﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups
{
    public class StringsWarmups
    {
        public string SayHi(string name)
        {
            return $"Hello {name}!";
        }
    }
}
