﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.Tests
{
    [TestFixture]
    class StringsWarmupsTests
    {
        [TestCase("Victor", "Hello Victor!")]
        public void SayHiTest(string name, string expected)
        {
            StringsWarmups sw = new StringsWarmups();
            string result = sw.SayHi(name);
            Assert.AreEqual(expected, result);
        }
    }
}
